const app = require('./routes/routes');
const server = require('./chatConfig');


const port = process.env.PORT || 3003;
server.listen(port, () => {
    console.log(`Chatting started at: ${port}`);
});

app.listen(3000, console.log.call(console, 'Server started at 3000.....'));