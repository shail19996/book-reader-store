module.exports = {
    check
};

//function to checks if user is loggedin or not
function check(req, res, next) {
    if (req.isAuthenticated()) {
        next();
    } else {
        res.status(400).json({ err: "Login First!" });
        res.end();
    }
}