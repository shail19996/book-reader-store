const app = require('./routes/routes');
const db = require('./dbConnection');
const squel = require("squel");
const http = require('http');
const server = http.Server(app);
const uuidv4 = require('uuid/v4');
const socketIO = require('socket.io');
const io = socketIO(server);
const chatController = require('./Chat/chat.controller');

io.on('connection', (socket) => {
    // console.log('a user connected');
    // socket.on('disconnect', function () {
    //     console.log('user disconnected');
    // });
    socket.on('new-message', (message) => {
        const id = uuidv4();
        message.chat_id = id;
        chatController.postChat(message);
        io.emit('new-message', message)
    });
});


module.exports = server;
