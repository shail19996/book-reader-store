const express = require('express');
const app = express();
const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
const FacebookStrategy = require('passport-facebook');
const session = require("express-session")
const bodyParser = require("body-parser");
const passportjs = require('../passport');

// Imports
// Controller
const userController = require('../User/user.controller');
const booksController = require('../Book/books.controller');
const fileController = require('../File/file.controller');
const notificationController = require('../Notification/notification.controller');
const feedbackController = require('../Feedback/feedback.controller');
const chatController = require('../Chat/chat.controller');

// Validation
const userValidation = require('../User/user.validation');
const booksValidation = require('../Book/books.validation');
const notificationValidation = require('../Notification/notification.validation');
const feedbackValidation = require('../Feedback/feedback.validation');

// Session
const sessionCheck = require('../session');

// middleware
var unless = function (req, res, next) {
    path = ['/downloadFile', '/uploadFile', '/logout',
        '/auth/google', '/auth/google/callback', '/auth/facebook',
        '/auth/facebook/callback', '/favicon.ico', '/chat']
    flag = 1;
    for (let i = 0; i < path.length; i++) {
        if (path[i] == req.path) {
            flag = 0;
        }
    }
    if (flag == 0) {
        next();
    } else {
        sessionCheck.check(req, res, next);
    }
};

app.use(express.static('public'));
app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Origin', req.headers.origin);
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept');
    if ('OPTIONS' == req.method) {
        res.send(200);
    } else {
        next();
    }
});

// app.use(express.static("public"));
app.use(session({ secret: "cats" }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(passport.initialize());
app.use(passport.session());
app.use(unless);


// End Points
// User
app.get('/auth/google', passport.authenticate('google', { scope: ['profile', 'email'] }));
app.get('/auth/google/callback', passport.authenticate('google', { failureRedirect: '/auth/google' }), userController.login);
app.get('/auth/facebook', passport.authenticate('facebook'));
app.get('/auth/facebook/callback', passport.authenticate('facebook', { failureRedirect: '/auth/facebook' }), userController.login);
app.get('/check', function (req, res) {
    res.status(200).json({ val: req.user })
});
app.get('/logout', (req, res) => { req.logout(); res.json('Logout').end(); });
app.get('/getData', userController.getData);
app.put('/updateData', userValidation.updateData, userController.updateData);

// Book
app.post('/addByForm', booksValidation.addByForm, booksController.addByForm);
app.get('/getAvailableBooks/:max/:min/:genre', booksController.getAvailableBooks);
app.get('/requestBook/:id', booksValidation.requestBook, booksController.requestBook);
app.get('/requestedBooks', booksController.requestedBooks);
app.get('/issuedBooks', booksController.issuedBooks);
app.get('/holdingBooks', booksController.holdingBooks);
app.get('/getBooks', booksController.getBooks);
app.delete('/books/:id', booksController.deleteBooks);
app.put('/books', booksController.updateBooks);

// Notification
app.get('/getNotification', notificationController.getNotification);
app.get('/rejectNotification/:email/:id', notificationValidation.rejectNotification, notificationController.rejectNotification);
app.get('/acceptNotification/:email/:id', notificationValidation.rejectNotification, notificationController.acceptNotification);
app.post('/returnRequest/:email', notificationValidation.getNotification, notificationController.returnRequest);
app.get('/acceptReturnRequest/:email/:id', notificationValidation.rejectNotification, notificationController.acceptReturnRequest);

// Feedback
app.get('/checkFeedback/:email/:id', feedbackValidation.checkFeedback, feedbackController.checkFeedback);
app.get('/getFeedback', feedbackController.getFeedback);
app.get('/getComments/:id', feedbackValidation.getComments, feedbackController.getComments);

// Download and Upload
app.get('/downloadFile', fileController.downloadFile)
app.post('/uploadFile', fileController.uploadFile, booksController.addByFile)

//Chat
app.get('/chat/:offset', chatController.getChat);
app.delete('/chat/:id', chatController.deleteChat);

// Error
// app.all('*', function (req, res) {
    // console.log("host:", req.headers)
    // console.log("url", req.url)
    // console.log("host:", req.headers.referer)
    // res.status(400).json('Invalid URL!!');
// })
//Export
module.exports = app;