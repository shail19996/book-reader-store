const express = require('express');
const app = express();
const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
const FacebookStrategy = require('passport-facebook');
const bodyParser = require("body-parser");


// Login By Google
const GOOGLE_CLIENT_ID = '527230742541-vsif6uid92mettfe1ll0csc1d03dl07b.apps.googleusercontent.com'
const GOOGLE_CLIENT_SECRET = 'LjmtycymQzY8FI8q1aFKDbzP'

passport.use(new GoogleStrategy({
    clientID: GOOGLE_CLIENT_ID,
    clientSecret: GOOGLE_CLIENT_SECRET,
    callbackURL: "/auth/google/callback"
},
    function (accessToken, refreshToken, profile, done) {
        if (profile) {
            let user = profile;
            return done(null, user);
        }
    }
));

// Login By Facebook
const FACEBOOK_APP_ID = "891725684362200";
const FACEBOOK_APP_SECRET = "2934a761e86de9a60d49c7c44fbb6f56";
passport.use(new FacebookStrategy({
    clientID: FACEBOOK_APP_ID,
    clientSecret: FACEBOOK_APP_SECRET,
    callbackURL: "/auth/facebook/callback",
    profileFields: ['id', 'displayName', 'photos', 'email']
},
    function (accessToken, refreshToken, profile, cb) {
        if (profile) {
            let user = profile;
            return cb(null, user);
        }
    }
));




//Session Management by Passport
passport.serializeUser(function (user, done) {
    done(null, user.emails[0].value);
});

passport.deserializeUser(function (id, done) {
    done(null, id);
});
