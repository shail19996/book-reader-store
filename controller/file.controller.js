const multer = require('multer');

module.exports = {
    downloadFile,
    uploadFile
};
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, '/home/shail/Desktop/demo/book-reader-store/public')
    },
    filename: (req, file, cb) => {
        let ex = file.originalname.split('.').pop();
        cb(null, file.fieldname + '-' + Date.now() + '.' + ex)
    }
});

const upload = multer({ storage: storage }).single('file');

function uploadFile(req, res, next) {
    upload(req, res, function (err) {
        if (err) {
            console.log("Error:", err)
            return res.end("Error uploading file." + err);
        } else {
            req.fileName = req.file.filename;
            console.log("*********************************************")
            console.log("req:", req.body)
            console.log("*********************************************")
            next();
            // res.send({
            //     success: true
            // })
            // res.redirect('http://localhost:3000/addByFile/' + req.file.filename);
        }
    });
}

function downloadFile(req, res) {
    var url = './public/template.xlsx'
    res.download(url, 'template.xlsx', function (err) {
        if (err) {
            console.log('something went wrong:', err);
        }
    });
}