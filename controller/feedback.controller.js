const model = require('../model/feedback.model');

module.exports = {
    checkFeedback,
    getFeedback,
    getComments
};


async function checkFeedback(req, res) {
    try {
        var response = await model.checkFeedback(req.params.email, req.params.id);
        res.status(200).json({ val: response })
    } catch (err) {
        res.status(400).json({ err: err });
    }
}

async function getFeedback(req, res) {
    try {
        console.log("===================================================")
        var response = await model.getFeedback(req.user);
        res.status(200).json({ val: response })
    } catch (err) {
        res.status(400).json({ err: err });
    }
}

async function getComments(req, res) {
    try {
        var response = await model.getComments(req.params.id);
        res.status(200).json({ val: response })
    } catch (err) {
        res.status(400).json({ err: err });
    }
}