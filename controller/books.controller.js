const model = require('../model/books.model');
var fs = require('fs');
var path = require('path');
module.exports = {
    addByForm,
    addByFile,
    getAvailableBooks,
    requestBook,
    requestedBooks,
    issuedBooks,
    holdingBooks,
    getBooks,
    deleteBooks
};

async function addByForm(req, res) {
    try {
        var response = await model.addByForm(req.user, req.body);
        res.status(200).json({ val: response })
    } catch (err) {
        res.status(400).json({ err: err });
    }
}

function fileDelete(fileName) {
    fs.unlink(fileName, function (err) {
        if (err)
            throw err;
        console.log('File deleted!');
    });
}

async function addByFile(req, res) {
    const XLSX = require('xlsx');
    var fileName = '/home/shail/Desktop/demo/book-reader-store/public/' + req.fileName;
    let workbook = XLSX.readFile(fileName);
    let sheet_name_list = workbook.SheetNames;
    data = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list]);
    if (path.extname(fileName) != '.xlsx') {
        console.log("format Not Proper");
        fileDelete(fileName);
        res.status(200).json({ val: 'File Format Not Proper!' })
    }
    else    if (data[0].Name === undefined || data[0].Name === null) {
        console.log("Data not proper");
        fileDelete(fileName);
        res.status(200).json({ val: 'File Content Not Proper!' })
    }
    if (data[0].Name && path.extname(fileName) == '.xlsx') {
        try {
            console.log("all ok")
            var response = await model.addByFile(req.body.UserEmail, data);
            fileDelete(fileName);
            res.status(200).json({ val: response })
        } catch (err) {
            res.status(400).json({ err: err });
        }
    }
}

async function getAvailableBooks(req, res) {
    try {
        var response = await model.getAvailableBooks(req.user);
        res.status(200).json({ val: response })
    } catch (err) {
        res.status(400).json({ err: err });
    }
}

async function requestBook(req, res) {
    try {
        var response = await model.requestBook(req.user, req.params.id);
        res.status(200).json({ val: response })
    } catch (err) {
        res.status(400).json({ err: err });
    }
}

async function requestedBooks(req, res) {
    try {
        var response = await model.requestedBooks(req.user);
        res.status(200).json({ val: response })
    } catch (err) {
        res.status(400).json({ err: err });
    }
}

async function issuedBooks(req, res) {
    try {
        var response = await model.issuedBooks(req.user);
        res.status(200).json({ val: response })
    } catch (err) {
        res.status(400).json({ err: err });
    }
}

async function holdingBooks(req, res) {
    try {
        var response = await model.holdingBooks(req.user);
        res.status(200).json({ val: response })
    } catch (err) {
        res.status(400).json({ err: err });
    }
}

async function getBooks(req, res) {
    try {
        var response = await model.getBooks(req.user);
        res.status(200).json({ val: response })
    } catch (err) {
        res.status(400).json({ err: err });
    }
}

async function deleteBooks(req, res) {
    try {
        var response = await model.deleteBooks(req.params.id);
        res.status(200).json({ val: response })
    } catch (err) {
        res.status(400).json({ err: err });
    }
}