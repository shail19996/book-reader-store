const model = require('../model/notification.model');

module.exports = {
    getNotification,
    rejectNotification,
    acceptNotification,
    returnRequest,
    acceptReturnRequest
};

async function getNotification(req, res) {
    try {
        const response = await model.getNotification(req.user);
        res.status(200).json({ val: response })
    } catch (err) {
        res.status(400).json({ err: err });
    }
}

async function rejectNotification(req, res) {
    try {
        const response = await model.rejectNotification(req.params.email, req.params.id);
        res.status(200).json({ val: response })
    } catch (err) {
        res.status(400).json({ err: err });
    }
}

async function acceptNotification(req, res) {
    try {
        const response = await model.acceptNotification(req.params.email, req.params.id);
        res.status(200).json({ val: response })
    } catch (err) {
        res.status(400).json({ err: err });
    }
}

async function returnRequest(req, res) {
    try {
        const response = await model.returnRequest(req.body, req.params.email);
        res.status(200).json({ val: response })
    } catch (err) {
        res.status(400).json({ err: err });
    }
}

async function acceptReturnRequest(req, res) {
    try {
        const response = await model.acceptReturnRequest(req.params.email, req.params.id);
        res.status(200).json({ val: response })
    } catch (err) {
        res.status(400).json({ err: err });
    }
}