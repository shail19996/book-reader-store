const model = require('../model/user.model');

module.exports = {
    login,
    getData,
    updateData
};

async function login(req, res) {
    try {
        var response = await model.login(req.user);
        res.redirect('http://localhost:4200/Home');
    } catch (err) {
        res.status(400).json({ err: err });
    }
}

async function getData(req, res) {
    try {
        var response = await model.getData(req.user);
        res.status(200).json({ val: response })
    } catch (err) {
        res.status(400).json({ err: err });
    }
}


async function updateData(req, res) {
    try {
        var response = await model.updateData(req.user, req.body);
        res.status(200).json({ val: response })
    } catch (err) {
        res.status(400).json({ err: err });
    }
}