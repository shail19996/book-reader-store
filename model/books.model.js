const Promise = require('promise');
const squel = require("squel");
const db = require('./db');

module.exports = {
    addByForm,
    addByFile,
    getAvailableBooks,
    requestBook,
    requestedBooks,
    issuedBooks,
    holdingBooks,
    getBooks,
    deleteBooks
};

async function addByForm(email, body) {
    try {
        var query = "INSERT INTO books.book (book_name, book_genres, book_details, user_email, book_taken) VALUES ( ";
        query = query + "'" + body.book_name.replace("'", "\\'") + "', ";
        query = query + "'" + body.book_genres.replace("'", "\\'") + "', ";
        query = query + "'" + body.book_details.replace("'", "\\'") + "', ";
        query = query + "'" + email + "', ";
        query = query + "'" + 'false' + "')";
        console.log("query:=", query);
        res1 = await db.poolQueryPromised(query);
        return "Success!!";
    } catch (err) {

        console.log("Error In: addByForm");
        throw new Error(err);
    }
}

async function addByFile(email, body) {
    var query = "INSERT INTO books.book (book_name, book_genres, book_details";
    query = query + ", user_email, book_taken) VALUES "
    for (i = 0; i < body.length; i++) {
        query = query + "( '" + body[i].Name.replace("'", "\\'") + "', ";
        if (body[i].Genres) {
            query = query + "'" + body[i].Genres.replace("'", "\\'") + "', ";
        } else {
            query += "'', "
        }
        if (body[i].Comments) {
            query = query + "'" + body[i].Comments.replace("'", "\\'") + "', ";
        } else {
            query += "'', "
        }
        query = query + "'" + email + "', ";
        if (i === (body.length - 1)) {
            query = query + "'" + 'false' + "')";
        } else {
            query = query + "'" + 'false' + "'), ";
        }
    }
    try {
        res1 = await db.poolQueryPromised(query);
        return "Success!!";
    } catch (err) {
        console.log("Error In: addByFile");
        throw new Error(err);
    }
}


async function getAvailableBooks(email) {
    try {
        var query = "SELECT AVG(books.feedback.feedback_stars) AS avg, books.book.book_id, books.book.book_name, "
        query += "books.book.book_genres, books.book.book_details , books.book.user_email FROM "
        query += "books.book "
        query += "LEFT JOIN books.feedback "
        query += "ON books.feedback.book_id = books.book.book_id "
        query += "WHERE books.book.book_taken = 'false' AND books.book.user_email <> '" + email + "' "
        query += "and books.book.book_id not in("
        query += "SELECT books.notification.book_id FROM books.notification where books.notification.notification_from='" + email + "') "
        query += "GROUP BY books.book.book_id;"
        console.log("Query:", query);
        res1 = await db.poolQueryPromised(query);
        if (res1) {
            return res1;
        }
        else {
            return Promise.reject("Empty Database!")
        }
    } catch (err) {
        if (err = "Empty Database!") {
            return Promise.reject("Empty Database!");
        }
        console.log("Error In: getAvailableBooks");
        throw new Error(err);
    }
}

async function requestBook(email, id) {
    try {
        var query = "SELECT * FROM books.notification WHERE book_id = " + id + " AND notification_from = '" + email + "' AND notification_type = 'request'";
        res1 = await db.poolQueryPromised(query);
        if (!res1.length) {
            var query = "SELECT book_name , user_email FROM books.book WHERE book_id = " + id;
            res1 = await db.poolQueryPromised(query);
            if (res1[0].user_email === email) {
                console.log("Lol you can not request for your own book!");
            } else {
                query = "INSERT INTO books.notification (notification_from , notification_to , notification_message , book_id , notification_type , visible) VALUES ( '" + email + "' , '" + res1[0].user_email + "' , '" + res1[0].book_name + " was requested by " + email + "' , " + id + " ,'request' , 'true')";
                res1 = await db.poolQueryPromised(query);
                query = "INSERT INTO books.request (request_by , book_id ) VALUES ('" + email + "' , " + id + " )";
                res1 = await db.poolQueryPromised(query);
                return "Can Request!";
            }
        }
        else {
            return Promise.reject("Already Requested!")
        }
    } catch (err) {
        if (err = "Already Requested!") {
            return Promise.reject("Already Requested!");
        }
        console.log("Error In: requestBook");
        throw new Error(err);
    }
}

async function requestedBooks(email) {
    try {
        var query = "SELECT books.request.request_by, books.book.user_email, books.book.book_id , books.book.book_name, books.book.book_genres, books.book.book_details FROM books.request INNER JOIN books.book ON books.request.book_id = books.book.book_id WHERE books.request.request_by = '" + email + "'";
        res1 = await db.poolQueryPromised(query);
        if (res1.length) {
            return res1;
        }
        else {
            return Promise.reject("Empty")
        }
    } catch (err) {
        if (err = "Empty") {
            return Promise.reject("Empty");
        }
        console.log("Error In: requestedBooks");
        throw new Error(err);
    }
}


async function issuedBooks(email) {
    try {
        var query = "SELECT "
        query += "books.issue.issue_to , books.book.user_email, books.book.book_id , books.book.book_name, books.book.book_genres, books.book.book_details "
        query += "FROM books.issue "
        query += "INNER JOIN books.book "
        query += "ON books.issue.book_id = books.book.book_id "
        query += "WHERE books.book.user_email = '" + email + "'"
        res1 = await db.poolQueryPromised(query);
        if (res1.length) {
            return res1;
        }
        else {
            return Promise.reject("Empty")
        }
    } catch (err) {
        if (err = "Empty") {
            return Promise.reject("Empty");
        }
        console.log("Error In: issuedBooks");
        throw new Error(err);
    }
}

async function holdingBooks(email) {
    try {
        var query = "SELECT "
        query += "books.book.user_email, books.book.book_id , books.book.book_name, books.book.book_genres, books.book.book_details, books.issue.feedback_check "
        query += "FROM books.issue "
        query += "INNER JOIN books.book "
        query += "ON books.issue.book_id = books.book.book_id "
        query += "WHERE books.issue.issue_to = '" + email + "'";
        res1 = await db.poolQueryPromised(query);
        query = "SELECT book_id , book_name, book_genres, book_details , user_email "
        query += "FROM books.book "
        query += "WHERE user_email = '" + email + "' AND book_taken = 'false'"
        res2 = await db.poolQueryPromised(query);
        res3 = res1.concat(res2);
        if (res3.length)
            return res3;
        else {
            return Promise.reject("Empty")
        }
    } catch (err) {
        if (err = "Empty") {
            return Promise.reject("Empty");
        }
        console.log("Error In: holdingBooks");
        throw new Error(err);
    }
}


async function getBooks(email) {
    try {
        var query = "SELECT * FROM books.book WHERE book_taken = 'false' AND user_email = '" + email + "'";
        res1 = await db.poolQueryPromised(query);
        if (res1.length) {
            return res1;
        }
        else {
            return Promise.reject("Empty Database!")
        }
    } catch (err) {
        if (err = "Empty Database!") {
            return Promise.reject("Empty Database!");
        }
        console.log("Error In: getBooks");
        throw new Error(err);
    }
}


async function deleteBooks(id) {
    try {
        var query1 = "DELETE FROM books.book WHERE book_id = " + id;
        var query2 = "DELETE FROM books.feedback WHERE book_id = " + id;
        var query3 = "DELETE FROM books.issue WHERE book_id = " + id;
        var query4 = "DELETE FROM books.notification WHERE book_id = " + id;
        var query5 = "DELETE FROM books.request WHERE book_id = " + id;
        Promise.all([db.poolQueryPromised(query1), db.poolQueryPromised(query2), db.poolQueryPromised(query3), db.poolQueryPromised(query4), db.poolQueryPromised(query5)]).then(function (values) {
            return "Success!";
        });
    } catch (err) {
        console.log("Error In: deleteBooks", err);
        throw new Error(err);
    }
}