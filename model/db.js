const mysql = require('mysql');

const con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "root",
    database: "books"
});


module.exports = {
    poolQueryPromised
};

function poolQueryPromised(query) {
    return new Promise(function (resolve, reject) {
        con.query(query, function (err, result, fields) {
            if (err) {
                reject(err);
                console.log(err);
            } else {
                resolve(result);
            }
        });
    });
}