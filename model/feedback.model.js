const Promise = require('promise');
const squel = require("squel");
const db = require('./db');

module.exports = {
    checkFeedback,
    getFeedback,
    getComments
};

async function checkFeedback(email, id) {
    try {
        var selectFeedback = squel.select()
            .from("books.feedback")
            .where("book_id = " + id)
            .where("feedback_from = '" + email + "'")
            .toString()
        res1 = await db.poolQueryPromised(selectFeedback);
        if (res1.length) {
            return Promise.reject("Feedback already given");
        } else {
            return "Feedback required!";
        }

    } catch (err) {
        if (err = "Feedback already given") {
            return Promise.reject("Feedback already given");
        }
        console.log("Error In: checkFeedback");
        throw new Error(err);
    }
}

async function getFeedback(email) {
    try {
        console.log("111111111111111111111111111111111111111111111111")
        var selectAll = "SELECT "
        selectAll += "AVG(books.feedback.feedback_stars) AS avg, books.book.book_name, books.book.book_id "
        selectAll += "FROM books.feedback "
        selectAll += "INNER JOIN books.book  "
        selectAll += "ON books.feedback.book_id = books.book.book_id  "
        selectAll += "WHERE books.book.user_email = '" + email + "' "
        selectAll += "GROUP BY books.book.book_id;";
        // var selectAll = squel.select()
        //     .field("AVG(books.feedback.feedback_stars)", "avg")
        //     .field("books.book.book_name")
        //     .field("books.book.book_id")
        //     .from("books.feedback")
        //     .join("books.book")
        //     .where("books.book.user_email = '" + email + "'")
        //     .group("books.book.book_id")
        //     .toString()
        console.log("**************************************************")
        console.log("query:", selectAll);
        res1 = await db.poolQueryPromised(selectAll);
        console.log("res1", res1, res1.length)
        if (res1.length) {
            return res1;
        } else {
            return Promise.reject("No Feedback Found")
        }
    } catch (err) {
        if (err = "No Feedback Found") {
            return Promise.reject("No Feedback Found");
        }
        console.log("Error In: getFeedback");
        throw new Error(err);
    }
}

async function getComments(id) {
    try {
        var selectFeedback = squel.select()
            .from("books.feedback")
            .where("book_id = " + id)
            .where("feedback_comments is NOT NULL")
            .toString()
        res1 = await db.poolQueryPromised(selectFeedback);
        if (res1.length) {
            return res1
        } else {
            return Promise.reject("No Comments Found")
        }
    } catch (err) {
        if (err = "No Comments Found") {
            return Promise.reject("No Comments Found");
        }
        console.log("Error In: getComments");
        throw new Error(err);
    }
}