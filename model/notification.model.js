const Promise = require('promise');
const squel = require("squel");
const db = require('./db');


module.exports = {
    getNotification,
    rejectNotification,
    acceptNotification,
    returnRequest,
    acceptReturnRequest
};


async function getNotification(email) {
    try {

        var selectNotification = squel.select()
            .from("books.notification")
            .where("notification_to = '" + email + "'")
            .where("visible = 'true'")
            .toString()

        res1 = await db.poolQueryPromised(selectNotification);
        if (res1.length) {
            return res1;
        }
        else {
            return Promise.reject("No Notification!")
        }
    } catch (err) {
        if (err = "No Notification!") {
            return Promise.reject("No Notification!");
        }
        console.log("Error In: getNotification");
        throw new Error(err);
    }
}


async function rejectNotification(email, id) {
    try {
        var deleteNotification = squel.delete()
            .from("books.notification")
            .where("book_id = " + id)
            .where("notification_from = '" + email + "'")
            .toString();

        var deleteRequest = squel.delete()
            .from("books.request")
            .where("book_id = " + id)
            .where("request_by = '" + email + "'")
            .toString();

        Promise.all(db.poolQueryPromised(deleteNotification), db.poolQueryPromised(deleteRequest))
            .then(
                res1 => {
                    return "Deleted";
                }, err => {
                    throw err;
                }
            )
    } catch (err) {
        console.log("Error In: rejectNotification");
        throw new Error(err);
    }
}

async function acceptNotification(email, id) {
    try {
        var insertIssue = squel.insert()
            .into("books.issue")
            .set("issue_to", email)
            .set("book_id", id)
            .toString()
        var deleteNotification = squel.delete()
            .from("books.notification")
            .where("book_id = " + id)
            .where("notification_from = '" + email + "'")
            .toString();

        var deleteRequest = squel.delete()
            .from("books.request")
            .where("book_id = " + id)
            .where("request_by = '" + email + "'")
            .toString();

        var updateBook = squel.update()
            .table("books.book")
            .set("book_taken", "true")
            .where("book_id = " + id)
            .toString()

        var updateNotification = squel.update()
            .table("books.notification")
            .set("visible", "false")
            .where("book_id = " + id)
            .toString()

        Promise.all(db.poolQueryPromised(insertIssue), db.poolQueryPromised(deleteNotification),
            db.poolQueryPromised(deleteRequest), db.poolQueryPromised(updateBook), db.poolQueryPromised(updateNotification))
            .then(
                res1 => {
                    return "success!!";
                },
                err => {
                    throw err;
                }
            )
    } catch (err) {
        console.log("Error In: acceptNotification");
        throw new Error(err);
    }
}

async function returnRequest(body, email) {
    try {
        var selectNotification = "SELECT * FROM books.notification WHERE book_id = " + body.book_id + " AND notification_from ='" + body.email + "'";
        res1 = await db.poolQueryPromised(selectNotification)
        if (!res1.length) {
            var insertNotification = squel.insert()
                .into("books.notification")
                .set("notification_from", body.email)
                .set("notification_to", email)
                .set("book_id", body.book_id)
                .set("notification_type", "return")
                .set("notification_message", body.email + " wants to return " + body.book_name)
                .set("visible", "true")
                .toString()

            if (body.feedback_count && body.feedback_check != 'Submitted') {
                if (body.feedback_comments) {
                    var insertFeedback = squel.insert()
                        .into("books.feedback")
                        .set("book_id", body.book_id)
                        .set("feedback_comments", body.feedback_comments)
                        .set("feedback_from", body.email)
                        .set("feedback_stars", body.feedback_count)
                        .toString()
                }
                else {
                    var insertFeedback = squel.insert()
                        .into("books.feedback")
                        .set("book_id", body.book_id)
                        .set("feedback_from", body.email)
                        .set("feedback_stars", body.feedback_count)
                        .toString()
                }
                var updateIssue = squel.update()
                    .table("books.issue")
                    .set("feedback_check", "Submitted")
                    .where("book_id = " + body.book_id)
                    .where("issue_to = " + "'" + body.email + "'")
                    .toString()
            }
            Promise.all(db.poolQueryPromised(insertNotification), db.poolQueryPromised(insertFeedback), db.poolQueryPromised(updateIssue))
                .then(
                    res1 => {

                    }, err => {
                        throw err;
                    }
                )
            return "Success!";
        }
    } catch (err) {
        console.log("Error In: returnRequest");
        throw new Error(err);
    }
}


async function acceptReturnRequest(email, id) {
    try {

        var updateBook = squel.update()
            .table("books.book")
            .set("book_taken", "false")
            .where("book_id = " + id)
            .toString()
        var deleteIssue = squel.delete()
            .from("books.issue")
            .where("book_id = " + id)
            .toString();
        var deleteNotification = squel.delete()
            .from("books.notification")
            .where("book_id = " + id)
            .where("notification_from = '" + email + "'")
            .toString();
        var updateNotification = squel.update()
            .table("books.notification")
            .set("visible", "true")
            .where("book_id = " + id)
            .toString()
        Promise.all(db.poolQueryPromised(updateBook), db.poolQueryPromised(deleteIssue),
            db.poolQueryPromised(deleteNotification), db.poolQueryPromised(updateNotification))
            .then(
                res1 => {
                    return "Success!";
                }, err => {
                    throw err;
                }
            )
    } catch (err) {
        console.log("Error In: acceptReturnRequest");
        throw new Error(err);
    }
}