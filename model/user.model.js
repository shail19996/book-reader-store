const Promise = require('promise');
const squel = require("squel");
const db = require('./db');

module.exports = {
    login,
    getData,
    updateData
};

async function login(user) {
    try {
        var selectUser = squel.select()
            .from("books.user")
            .where("user_email = '" + user.emails[0].value + "'")
            .toString()
        var res1 = await db.poolQueryPromised(selectUser);
        if (res1.length === 0) {
            var insertUser = squel.insert()
                .into("books.user")
                .set("user_name", "'" + user.displayName + "'")
                .set("user_email", "'" + user.emails[0].value + "'")
                .set("user_image", "'" + user.photos[0].value + "'")
                .toString()
            await db.poolQueryPromised(insertUser);
            return user.emails[0].value;
        }
        else {
            return user.emails[0].value;
        }
    } catch (err) {
        console.log("Error In: login");
        throw new Error(err);
    }
}

async function getData(email) {
    try {
        var selectUser = squel.select()
            .from("books.user")
            .where("user_email = '" + user.emails[0].value + "'")
            .toString()
        var res1 = await db.poolQueryPromised(selectUser);
        if (res1.length === 0) {
            return Promise.reject("Invalid ID");
        }
        else {
            var selectNotification = squel.select()
                .from("books.notification")
                .field("COUNT(*)", "count")
                .where("notification_to = '" + email + "'")
                .where("visible = 'true'")
                .toString()

            var count = await db.poolQueryPromised(selectNotification);
            res1[0].count = count[0].count;
            return res1;
        }
    } catch (err) {
        if (err = "Invalid ID") {
            return Promise.reject("Invalid ID");
        }
        console.log("Error In: getData");
        throw new Error(err);
    }
}

async function updateData(email, body) {
    var size = Object.keys(body).length;
    if (size) {
        var query = "UPDATE books.user SET ";
        var i = 1;
        for (key in body) {
            if (key === 'user_mobile') {
                query = query + key + " = " + body[key];
            } else {
                query = query + key + " = '" + body[key] + "'";
            }
            if (i != size) {
                query = query + " ,";
            }
            i++;
        }
        query = query + " WHERE user_email = '" + email + "'";
        try {
            await db.poolQueryPromised(query);
            return "Success!!";
        } catch (err) {
            console.log("Error In: updateData");
            throw new Error(err);
        }
    } else {
        return "No Input";
    }
}
