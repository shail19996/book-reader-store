const Joi = require('joi');

module.exports = {
    getNotification,
    rejectNotification
}

//Schema
const getNotification_Schema = Joi.object().keys({
    email: Joi.string().email().required()
})

const rejectNotification_Schema = Joi.object().keys({
    email: Joi.string().email().required(),
    id: Joi.number().min(1).required()
})

//Common Function
function commmonFunctionCall(objectValue, schemaValue, res, next) {
    const result = Joi.validate(objectValue, schemaValue);
    if (result.error) {
        res.status(400).json({ err: result.error.details[0].message });
        res.end;
    }
    else {
        next();
    }
}


//Functions to validate input data
function getNotification(req, res, next) {
    commmonFunctionCall(req.params, getNotification_Schema, res, next);
}

function rejectNotification(req, res, next) {
    commmonFunctionCall(req.params, rejectNotification_Schema, res, next);
}