const Joi = require('joi');

module.exports = {
    updateData
}

//Schema
const updateData_Schema = Joi.object().keys({
    user_mobile: Joi.number().integer().min(1000000000).max(9999999999).allow('').optional(),
	user_address: Joi.string().allow('').optional(),
	user_city: Joi.string().allow('').optional()
})

//Functions to validate input data
function updateData(req, res, next) {
    const result = Joi.validate({ user_mobile: req.body.user_mobile, user_address: req.body.user_address, user_city: req.body.user_city }, updateData_Schema);
    if (result.error) {
        res.status(400).json({ err: result.error.details[0].message });
        res.end;
    }
    else {
        next();
    }
}