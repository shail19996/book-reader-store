const Joi = require('joi');

module.exports = {
    getComments
}

//Schema
const getComments_Schema = Joi.object().keys({
    id: Joi.number().min(1).required()
})

//Functions to validate input data
function getComments(req, res, next) {
    const result = Joi.validate({ id: req.params.id }, getComments_Schema);
    if (result.error) {
        res.status(400).json({ err: result.error.details[0].message });
        res.end;
    }
    else {
        next();
    }
}