const Joi = require('joi');

module.exports = {
    getNotification,
    rejectNotification
}

//Schema
const getNotification_Schema = Joi.object().keys({
    email: Joi.string().email().required()
})

const rejectNotification_Schema = Joi.object().keys({
    email: Joi.string().email().required(),
    id: Joi.number().min(1).required()
})

//Functions to validate input data
function getNotification(req, res, next) {
    const result = Joi.validate({ email: req.params.email }, getNotification_Schema);
    if (result.error) {
        res.status(400).json({ err: result.error.details[0].message });
        res.end;
    }
    else {
        next();
    }
}

function rejectNotification(req, res, next) {
    const result = Joi.validate({ email: req.params.email , id: req.params.id }, rejectNotification_Schema);
    if (result.error) {
        res.status(400).json({ err: result.error.details[0].message });
        res.end;
    }
    else {
        next();
    }
}