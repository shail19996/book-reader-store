const Joi = require('joi');

module.exports = {
    requestBook,
    addByForm
}

//Schema
const requestBook_Schema = Joi.object().keys({
    id: Joi.number().min(1).required()
})

const addByForm_Schema = Joi.object().keys({
    book_name: Joi.string().max(45).allow('').optional(),
    book_genres: Joi.string().max(20).allow('').optional(),
    book_details: Joi.string().max(1431655765).allow('').optional()
})

//Functions to validate input data

function requestBook(req, res, next) {
    const result = Joi.validate({ id: req.params.id }, requestBook_Schema);
    if (result.error) {
        res.status(400).json({ err: result.error.details[0].message });
        res.end;
    }
    else {
        next();
    }
}

function addByForm(req, res, next) {
    const result = Joi.validate({ book_name: req.body.book_name, book_genres: req.body.book_genres, book_details: req.body.book_details }, addByForm_Schema);
    if (result.error) {
        res.status(400).json({ err: result.error.details[0].message });
        res.end;
    }
    else {
        next();
    }
}