const Promise = require('promise');
const squel = require("squel");
const db = require('../dbConnection');

module.exports = {
    addByForm,
    addByFile,
    getAvailableBooks,
    requestBook,
    requestedBooks,
    issuedBooks,
    holdingBooks,
    getBooks,
    deleteBooks,
    updateBooks
};

async function addByForm(email, body) {
    try {
        // re for adding \\ before all '
        let re = new RegExp("'", 'g');
        // query for adding book by form
        let query = "INSERT INTO books.book (book_name, book_genres, book_details, user_email, book_taken) VALUES ( ";
        query = query + "'" + body.book_name.replace(re, "\\'") + "', ";
        query = query + "'" + body.book_genres.replace(re, "\\'") + "', ";
        query = query + "'" + body.book_details.replace(re, "\\'") + "', ";
        query = query + "'" + email + "', ";
        query = query + "'" + 'false' + "')";
        res1 = await db.poolQueryPromised(query);
        return "Success!!";
    } catch (err) {

        console.log("Error In: addByForm");
        throw new Error(err);
    }
}

async function addByFile(email, body) {
    let re = new RegExp("'", 'g');
    let query = "INSERT INTO books.book (book_name, book_genres, book_details";
    query = query + ", user_email, book_taken) VALUES "
    for (let i = 0; i < body.length; i++) {
        query = query + "( '" + body[i].Name.replace(re, "\\'") + "', ";
        if (body[i].Genres) {
            query = query + "'" + body[i].Genres.replace(re, "\\'") + "', ";
        } else {
            query += "'', "
        }
        if (body[i].Comments) {
            query = query + "'" + body[i].Comments.replace(re, "\\'") + "', ";
        } else {
            query += "'', "
        }
        query = query + "'" + email + "', ";
        if (i === (body.length - 1)) {
            query = query + "'" + 'false' + "')";
        } else {
            query = query + "'" + 'false' + "'), ";
        }
    }
    try {
        res1 = await db.poolQueryPromised(query);
        return "Success!!";
    } catch (err) {
        console.log("Error In: addByFile");
        throw new Error(err);
    }
}


async function getAvailableBooks(email, params) {
    try {
        // query for returning all the available and non requestied books to a user
        var query = "SELECT AVG(books.feedback.feedback_stars) AS avg, books.book.book_id, books.book.book_name, "
        query += "books.book.book_genres, books.book.book_details , books.book.user_email FROM "
        query += "books.book "
        query += "LEFT JOIN books.feedback "
        query += "ON books.feedback.book_id = books.book.book_id "
        query += "WHERE books.book.book_taken = 'false' AND books.book.user_email <> '" + email + "' "
        if (params.genre != 'null') {
            query += "AND books.book.book_genres = '" + params.genre + "' "
        }
        // shows initial condition and hence all return all the records even which are not rated
        if (!(params.min == '0' && params.max == '5')) {
            query += "AND books.feedback.feedback_stars >= " + parseInt(params.min, 10) + " "
            query += "AND books.feedback.feedback_stars <= " + parseInt(params.max, 10) + " "
        }
        query += "and books.book.book_id not in("
        query += "SELECT books.notification.book_id FROM books.notification where books.notification.notification_from='" + email + "') "
        query += "GROUP BY books.book.book_id;"
        res1 = await db.poolQueryPromised(query);
        if (res1) {
            return res1;
        }
        else {
            return Promise.reject("Empty Database!")
        }
    } catch (err) {
        if (err = "Empty Database!") {
            return Promise.reject("Empty Database!");
        }
        console.log("Error In: getAvailableBooks");
        throw new Error(err);
    }
}

async function requestBook(email, id) {
    try {
        // query to check if book is requested or not
        var query = "SELECT * FROM books.notification WHERE book_id = " + id + " AND notification_from = '" + email + "' AND notification_type = 'request'";
        res1 = await db.poolQueryPromised(query);
        if (!res1.length) {
            // query to check if user is not requesting his own book
            var query = "SELECT book_name , user_email FROM books.book WHERE book_id = " + id;
            res1 = await db.poolQueryPromised(query);
            if (res1[0].user_email === email) {
                console.log("Lol you can not request for your own book!");
            } else {
                // query for inserting notification and adding book to requested table
                query = "INSERT INTO books.notification (notification_from , notification_to , notification_message , book_id , notification_type , visible) VALUES ( '" + email + "' , '" + res1[0].user_email + "' , '" + res1[0].book_name + " was requested by " + email + "' , " + id + " ,'request' , 'true')";
                res1 = await db.poolQueryPromised(query);
                query = "INSERT INTO books.request (request_by , book_id ) VALUES ('" + email + "' , " + id + " )";
                res1 = await db.poolQueryPromised(query);
                return "Can Request!";
            }
        }
        else {
            return Promise.reject("Already Requested!")
        }
    } catch (err) {
        if (err = "Already Requested!") {
            return Promise.reject("Already Requested!");
        }
        console.log("Error In: requestBook");
        throw new Error(err);
    }
}

async function requestedBooks(email) {
    try {
        // query for returning all the requested books by a user
        var query = "SELECT books.request.request_by, books.book.user_email, books.book.book_id , books.book.book_name, books.book.book_genres, books.book.book_details FROM books.request INNER JOIN books.book ON books.request.book_id = books.book.book_id WHERE books.request.request_by = '" + email + "'";
        res1 = await db.poolQueryPromised(query);
        if (res1.length) {
            return res1;
        }
        else {
            return Promise.reject("Empty")
        }
    } catch (err) {
        if (err = "Empty") {
            return Promise.reject("Empty");
        }
        console.log("Error In: requestedBooks");
        throw new Error(err);
    }
}


async function issuedBooks(email) {
    try {
        // query for getting all the issued books or a perticular user
        var query = "SELECT "
        query += "books.issue.issue_to , books.book.user_email, books.book.book_id , books.book.book_name, books.book.book_genres, books.book.book_details "
        query += "FROM books.issue "
        query += "INNER JOIN books.book "
        query += "ON books.issue.book_id = books.book.book_id "
        query += "WHERE books.book.user_email = '" + email + "'"
        res1 = await db.poolQueryPromised(query);
        if (res1.length) {
            return res1;
        }
        else {
            return Promise.reject("Empty")
        }
    } catch (err) {
        if (err = "Empty") {
            return Promise.reject("Empty");
        }
        console.log("Error In: issuedBooks");
        throw new Error(err);
    }
}

async function holdingBooks(email) {
    try {
        // query for getting all the books on hand of a perticular user
        var query = "SELECT "
        query += "books.book.user_email, books.book.book_id , books.book.book_name, books.book.book_genres, books.book.book_details, books.issue.feedback_check "
        query += "FROM books.issue "
        query += "INNER JOIN books.book "
        query += "ON books.issue.book_id = books.book.book_id "
        query += "WHERE books.issue.issue_to = '" + email + "'";
        res1 = await db.poolQueryPromised(query);
        query = "SELECT book_id , book_name, book_genres, book_details , user_email "
        query += "FROM books.book "
        query += "WHERE user_email = '" + email + "' AND book_taken = 'false'"
        res2 = await db.poolQueryPromised(query);
        res3 = res1.concat(res2);
        if (res3.length)
            return res3;
        else {
            return Promise.reject("Empty")
        }
    } catch (err) {
        if (err = "Empty") {
            return Promise.reject("Empty");
        }
        console.log("Error In: holdingBooks");
        throw new Error(err);
    }
}


async function getBooks(email) {
    try {
        // query for getting all the available books for a perticular user
        var query = "SELECT * FROM books.book WHERE book_taken = 'false' AND user_email = '" + email + "'";
        res1 = await db.poolQueryPromised(query);
        if (res1.length) {
            return res1;
        }
        else {
            return Promise.reject("Empty Database!")
        }
    } catch (err) {
        if (err = "Empty Database!") {
            return Promise.reject("Empty Database!");
        }
        console.log("Error In: getBooks");
        throw new Error(err);
    }
}


async function deleteBooks(id) {
    try {
        // query for deleting book from book, feedback, issue, notification and request tables
        var query1 = "DELETE FROM books.book WHERE book_id = " + id;
        var query2 = "DELETE FROM books.feedback WHERE book_id = " + id;
        var query3 = "DELETE FROM books.issue WHERE book_id = " + id;
        var query4 = "DELETE FROM books.notification WHERE book_id = " + id;
        var query5 = "DELETE FROM books.request WHERE book_id = " + id;
        Promise.all([db.poolQueryPromised(query1), db.poolQueryPromised(query2), db.poolQueryPromised(query3), db.poolQueryPromised(query4), db.poolQueryPromised(query5)]).then(function (values) {
            return "Success!";
        });
    } catch (err) {
        console.log("Error In: deleteBooks", err);
        throw new Error(err);
    }
}


async function updateBooks(body) {
    let re = new RegExp("'", 'g');
    // query for updating book details
    var updateBook = squel.update()
        .table("books.book")
        .set("book_name", (body.book_name.replace(re, "\\'")))
        .set("book_genres", body.book_genres)
        .set("book_details", (body.book_details.replace(re, "\\'")))
        .where("book_id = " + body.book_id)
        .toString()
    await db.poolQueryPromised(updateBook);
    return 'done';
}