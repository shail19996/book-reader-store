const Joi = require('joi');

module.exports = {
    requestBook,
    addByForm
}

//Schema
const requestBook_Schema = Joi.object().keys({
    id: Joi.number().min(1).required()
})

const addByForm_Schema = Joi.object().keys({
    book_name: Joi.string().max(45).allow('').optional(),
    book_genres: Joi.string().max(20).allow('').optional(),
    book_details: Joi.string().max(1431655765).allow('').optional(),
    user_email: Joi.string().email().required()
})

//Common Function
function commmonFunctionCall(objectValue, schemaValue, res, next) {
    const result = Joi.validate(objectValue, schemaValue);
    if (result.error) {
        res.status(400).json({ err: result.error.details[0].message });
        res.end;
    }
    else {
        next();
    }
}

//Functions to validate input data
function requestBook(req, res, next) {
    commmonFunctionCall(req.params, requestBook_Schema, res, next);
}

function addByForm(req, res, next) {
    console.log(req.body,"Body")
    commmonFunctionCall(req.body, addByForm_Schema, res, next);
}