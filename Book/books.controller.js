const model = require('./books.model');
const fs = require('fs');
const path = require('path');
module.exports = {
    addByForm,
    addByFile,
    getAvailableBooks,
    requestBook,
    requestedBooks,
    issuedBooks,
    holdingBooks,
    getBooks,
    deleteBooks,
    updateBooks
};

// Function to add book by form
async function addByForm(req, res) {
    try {
        const response = await model.addByForm(req.user, req.body);
        res.status(200).json({ val: response })
    } catch (err) {
        res.status(400).json({ err: err });
    }
}

// Function to Delete Uploaded File
function fileDelete(fileName) {
    fs.unlink(fileName, function (err) {
        if (err)
            throw err;
    });
}

// Function to Get Data from uploaded .xlsx file
// and add it to the database
async function addByFile(req, res) {
    const XLSX = require('xlsx');
    const fileName = '/home/shail/Desktop/demo/book-reader-store/public/' + req.fileName;
    let workbook = XLSX.readFile(fileName);
    let sheet_name_list = workbook.SheetNames;
    data = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list]);
    // Checks if file is .xlsx or not
    if (path.extname(fileName) != '.xlsx') {
        fileDelete(fileName);
        res.status(200).json({ val: 'File Format Not Proper!' })
    }
    // checks if file content is proper or not
    else if (data[0].Name === undefined || data[0].Name === null) {
        fileDelete(fileName);
        res.status(200).json({ val: 'File Content Not Proper!' })
    }
    try {
        const response = await model.addByFile(req.body.UserEmail, data);
        fileDelete(fileName);
        res.status(200).json({ val: response })
    } catch (err) {
        res.status(400).json({ err: err });
    }
}

// Function to get Available Books to request
async function getAvailableBooks(req, res) {
    try {
        const response = await model.getAvailableBooks(req.user, req.params);
        res.status(200).json({ val: response })
    } catch (err) {
        res.status(400).json({ err: err });
    }
}

// Function to request book
async function requestBook(req, res) {
    try {
        const response = await model.requestBook(req.user, req.params.id);
        res.status(200).json({ val: response })
    } catch (err) {
        res.status(400).json({ err: err });
    }
}

// Function to Show Requested Books
async function requestedBooks(req, res) {
    try {
        const response = await model.requestedBooks(req.user);
        res.status(200).json({ val: response })
    } catch (err) {
        res.status(400).json({ err: err });
    }
}

// Function to Show Issued Books
async function issuedBooks(req, res) {
    try {
        const response = await model.issuedBooks(req.user);
        res.status(200).json({ val: response })
    } catch (err) {
        res.status(400).json({ err: err });
    }
}

// Function to Show Books on hand
async function holdingBooks(req, res) {
    try {
        const response = await model.holdingBooks(req.user);
        res.status(200).json({ val: response })
    } catch (err) {
        res.status(400).json({ err: err });
    }
}

// Function to get all available books
async function getBooks(req, res) {
    try {
        const response = await model.getBooks(req.user);
        res.status(200).json({ val: response })
    } catch (err) {
        res.status(400).json({ err: err });
    }
}

// Function to Delete specific book
async function deleteBooks(req, res) {
    try {
        const response = await model.deleteBooks(req.params.id);
        res.status(200).json({ val: response })
    } catch (err) {
        res.status(400).json({ err: err });
    }
}

// Function to Update specific book details
async function updateBooks(req, res) {
    try {
        console.log("Update Call")
        const response = await model.updateBooks(req.body);
        res.status(200).json({ val: response })
    } catch (err) {
        res.status(400).json({ err: err });
    }
}