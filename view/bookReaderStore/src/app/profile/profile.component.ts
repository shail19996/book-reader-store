import { Component, OnInit } from '@angular/core';
import { Httpuserservice } from '../service/httpUserService';
import { Router } from '@angular/router';
import { NotifierService } from 'angular-notifier';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  //Variable Declaration
  mobileMessage: string = "";
  buttonFlag: boolean = false;
  user = {
    user_name: '',
    user_email: '',
    user_mobile: '',
    user_address: '',
    user_city: '',
    count: ''
  }
  objCopy = {}
  private notifier: NotifierService;
  constructor(private httpuserservice: Httpuserservice,
    private spinnerService: Ng4LoadingSpinnerService,
    private router: Router,
    notifier: NotifierService) {
    this.spinnerService.show();
    this.notifier = notifier;
  }
  ngOnInit() {
    this.httpuserservice.getData()
      .subscribe(
        (res) => {
          this.user = res.val[0]
          //a dupliacte copy is made to compare the value at a later stage
          this.objCopy = Object.assign({}, this.user);
          this.spinnerService.hide();
        },
        (err) => {
          this.notifier.notify('error', 'Whoops, something went wrong. Probably.');
        }
      )
  }

  Cancel() {
    this.router.navigate(['/Home']);
  }

  //Function to check if two objects are same or not
  isEquivalent(a, b) {
    var aProps = Object.getOwnPropertyNames(a);
    var bProps = Object.getOwnPropertyNames(b);

    //checks for object length
    if (aProps.length != bProps.length) {
      return false;
    }

    //checks values
    for (var i = 0; i < aProps.length; i++) {
      var propName = aProps[i];
      if (a[propName] !== b[propName]) {
        return false;
      }
    }

    return true;
  }

  Save() {
    //if condition check if data is changed or not. 
    this.spinnerService.show();
    if (this.isEquivalent(this.objCopy, this.user)) {
      //no change in data and hence no need to send request to update data
      this.spinnerService.hide();
      this.notifier.notify('default', 'Their was no change in data');
    } else {
      //data was changed and hence request is send to update the data in database
      delete this.user.count;
      if (!this.user.user_mobile)
        this.user.user_mobile = '';
      if (!this.user.user_address)
        this.user.user_address = '';
      if (!this.user.user_city)
        this.user.user_city = '';

      this.httpuserservice.updateData(this.user)
        .subscribe(
          (res) => {
            this.notifier.notify('success', 'Data Updated!');
            this.objCopy = Object.assign({}, this.user);
            this.spinnerService.hide();
          },
          (err) => {
            console.log(err)
            this.spinnerService.hide();
            this.notifier.notify('error', 'Whoops, something went wrong. Probably.');
          }
        )
    }
  }

  //function responsible for checking if the Mobile Number column contains proper number of character or not
  onMobileInput(event: Event) {
    let val;
    val = (<HTMLInputElement>event.target).value;
    var pattern = new RegExp("\\d{10}$");
    this.buttonFlag = false;
    if (!pattern.test(val) && val.length > 0)
      this.buttonFlag = true;
  }

  //function responsible for checking if the Address column contains proper number of character or not
  //and if the number exceeds than the input will be removed
  onAddressInput(event: Event) {
    let val;
    val = (<HTMLInputElement>event.target).value;
    if (val.length > 99) {
      (<HTMLInputElement>event.target).value = val.substring(0, val.length - 1);
    }
  }

  //function responsible for checking if the City column contains proper number of character or not
  //and if the number exceeds than the input will be removed
  onCityInput(event: Event) {
    let val;
    val = (<HTMLInputElement>event.target).value;
    if (val.length > 39) {
      (<HTMLInputElement>event.target).value = val.substring(0, val.length - 1);
    }
  }

}
