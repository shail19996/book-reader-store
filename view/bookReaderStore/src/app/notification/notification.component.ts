import { Component, OnInit } from '@angular/core';
import { Httpnotificationservice } from '../service/httpNotificationService';
import { TemplateComponent } from '../template/template.component';
import { NotifierService } from 'angular-notifier';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css'],
  providers: [TemplateComponent]
})
export class NotificationComponent implements OnInit {

  //Variable Declaration
  notifications = [{}];
  show: boolean = true;
  message = "No Notifications Available";
  private notifier: NotifierService;
  constructor(private httpnotificationservice: Httpnotificationservice,
    private spinnerService: Ng4LoadingSpinnerService,
    private template: TemplateComponent,
    notifier: NotifierService) {
    this.spinnerService.show();
    this.notifier = notifier;
  }


  ngOnInit() {
    this.httpnotificationservice.getNotification()
      .subscribe(
        res => {
          this.notifications = res.val;
          this.spinnerService.hide();
        },
        err => {
          this.show = false;
          this.spinnerService.hide();
        }
      )
  }

  //function called after notification is accepted for giving book
  Accept(id, name) {
    this.spinnerService.show();
    this.httpnotificationservice.acceptNotification(name, id)
      .subscribe(
        res => {
          this.ngOnInit();
          this.template.ngOnInit();
          this.spinnerService.hide();
          this.notifier.notify('success', 'Thanks for giving me the book!!');
        },
        err => {
          this.notifier.notify('error', 'Whoops, something went wrong. Probably.');
        }
      )
  }

  //function called after notification is rejected for giving book
  Reject(id, name) {
    this.spinnerService.show();
    this.httpnotificationservice.rejectNotification(name, id)
      .subscribe(
        res => {
          this.ngOnInit();
          this.template.ngOnInit();
          this.spinnerService.hide();
          this.notifier.notify('error', 'Not a problem!!');
        },
        err => {
          this.notifier.notify('error', 'Whoops, something went wrong. Probably.');
        }
      )
  }


  //function called after book is received back by the owner
  Received(id, email) {
    this.spinnerService.show();
    this.httpnotificationservice.acceptReturnRequest(email, id)
      .subscribe(
        res => {
          this.ngOnInit();
          this.template.ngOnInit();
          this.spinnerService.hide();
          this.notifier.notify('success', 'Thanks for giving me the book!!');
        },
        err => {
          this.notifier.notify('error', 'Whoops, something went wrong. Probably.');
        }
      )
  }
}
