import { Component, OnInit, OnDestroy } from '@angular/core';
import { Httpfeedbackservice } from '../service/httpFeedbackService';
import { Subject } from 'rxjs';
import { Router } from '@angular/router'
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.css']
})
export class FeedbackComponent implements OnInit, OnDestroy {
  constructor(private Httpfeedbackservice: Httpfeedbackservice,
    private spinnerService: Ng4LoadingSpinnerService,
    private router: Router) {
    this.spinnerService.show();
  }

  //Variable Declaration
  modalShow: boolean = false;
  popupErrorMessage = 'No Comments to Read!';
  show: boolean = true;
  showmessage: string = "No feedbacks available Currently!"
  p: number = 1;
  books = [{}];
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();

  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      processing: true
    };

    this.Httpfeedbackservice.getFeedback()
      .subscribe(
        res => {
          if (res.val && res.val.length) {
            this.books = res.val;
            this.dtTrigger.next();
            this.spinnerService.hide();
          } else {
            this.show = false;
          }
        },
        err => {
          this.show = false;
        }
      )
  }


  showComments(id) {
    this.modalShow = false;
    this.Httpfeedbackservice.getComments(id)
      .subscribe(
        res => {
          //redirected to a new page to read comments
          this.router.navigate(['/ReadComment/Feedback', id]);
        }, err => {
          this.modalShow = true;
        }
      )
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }
}
