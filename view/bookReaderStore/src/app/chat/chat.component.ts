import { Component, OnInit, HostListener } from '@angular/core';
import { ChatService } from '../service/chat.service';
import { Httpuserservice } from '../service/httpUserService';
import { Httpchatservice } from '../service/httpChatService';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {
  message: string;
  messageObj = {
    message: null,
    sender: null,
    date: null,
    time: null,
    user_image: null
  }
  messages = [{
    chat_id: null,
    message: null,
    sender: null,
    date: null,
    time: null,
    user_image: null
  }];
  name: string;
  LoadMoreShow: boolean = true;
  offset: number = 0;
  flag: boolean = false;
  image_url: string;

  constructor(private httpuserservice: Httpuserservice,
    private httpchatservice: Httpchatservice,
    private spinnerService: Ng4LoadingSpinnerService,
    private chatService: ChatService) {
    this.spinnerService.show();
  }

  // appending date , time and sender to the message
  sendMessage() {
    if (this.message) {
      this.messageObj.message = this.message;
      this.messageObj.sender = this.name;
      var str = new Date().toLocaleString("es-US")
      str = str.replace(' ', ',');
      var temp = str.split(",")
      this.messageObj.date = temp[0];
      this.messageObj.time = temp[1].replace('p. ', 'P.').replace('a. ', 'A.').replace('m', 'M')
      this.messageObj.user_image = this.image_url;
      this.chatService.sendMessage(this.messageObj);
      this.message = '';
    }
  }

  ngOnInit() {

    // Get user name
    // this.httpuserservice.check()
    //   .subscribe(
    //     res => {
    //       this.name = res.val;
    //     }
    // )

    this.httpuserservice.getData()
      .subscribe(
        res2 => {
          this.name = res2.val[0].user_email;
          this.image_url = res2.val[0].user_image;
        }
      )
    // Get Chat History form database
    this.httpchatservice.getChat(this.offset)
      .subscribe(
        res1 => {
          this.messages = res1.val;
          this.spinnerService.hide();
        }
      )

    // get the new broadcasted messages
    this.chatService.getMessages()
      .subscribe((message) => {
        this.messages.unshift(message);
      });
  }

  // checking message input
  onMessageInput(event: Event) {
    let val;
    val = (<HTMLInputElement>event.target).value;
    if (val.length > 149) {
      (<HTMLInputElement>event.target).value = val.substring(0, val.length - 1);
    }
  }

  // removing message
  deleteMessage(id) {
    this.httpchatservice.deleteChat(id)
      .subscribe(
        res => {
          for (let i = 0; i < this.messages.length; i++) {
            if (this.messages[i].chat_id == id) {
              this.messages.splice(i, 1);
            }
          }
        }
      )
  }

  // Load next few mesasges
  loadMore() {
    this.spinnerService.show();
    this.offset = this.offset + 10;
    this.httpchatservice.getChat(this.offset)
      .subscribe(
        res1 => {
          if (res1.val && res1.val.length > 0) {
            this.messages = this.messages.concat(res1.val)
            this.spinnerService.hide();
            this.LoadMoreShow = true;
            // checks if the last response data was less than 10 or not and
            // if it is it will hide the Lode More Button because no more messages are available
            if (res1.val.length != 10) {
              this.LoadMoreShow = false;
            }
          } else {
            this.LoadMoreShow = false;
            this.spinnerService.hide();
          }
        }
      )
  }

  // Detects end of page and call the function to load new data
  @HostListener("window:scroll", [])
  onScroll(): void {
    if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight && this.LoadMoreShow == true) {
      this.loadMore()
    }
  }
}