import {
    CanActivate,
    ActivatedRouteSnapshot,
    RouterStateSnapshot,
    Router
} from '@angular/router'
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Httpuserservice } from './service/httpUserService';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private httpuserservice: Httpuserservice,
        private router: Router) {
    }

    checkForLogin() {
        return new Promise((resolve, reject) => {
            this.httpuserservice.check()
                .subscribe(
                    (res) => {
                        resolve(res);
                    },
                    (err) => {
                        reject(err);
                    }
                )
        })
    };

    canActivate(route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        return this.checkForLogin()
            .then(res1 => {
                return Promise.resolve(true);
            }).catch(err1 => {
                this.router.navigate(['/']);
                return Promise.resolve(false);
            })
    }
}