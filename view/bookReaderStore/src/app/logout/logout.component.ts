import { Component, OnInit } from '@angular/core';
import { Httpuserservice } from '../service/httpUserService';
import { Router } from '@angular/router';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  constructor(private httpuserservice: Httpuserservice,
    private spinnerService: Ng4LoadingSpinnerService,
    private router: Router) {
    this.spinnerService.show();
  }

  ngOnInit() {
    this.httpuserservice.logout()
      .subscribe(
        (res) => {
          //redirected to login page on successfull logout
          this.spinnerService.hide();
          this.router.navigate(['/']);
        },
        (err) => {
          //If an error occures while logout than it is redirected to home page
          this.spinnerService.hide();
          this.router.navigate(['/Home']);
        }
      )
  }

}
