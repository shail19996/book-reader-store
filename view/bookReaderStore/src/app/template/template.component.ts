import { Component, OnInit } from '@angular/core';
import { Httpuserservice } from '../service/httpUserService';
import * as $ from 'jquery';

@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.css']
})
export class TemplateComponent implements OnInit {

  constructor(private httpuserservice: Httpuserservice) { }
  user_name = '';
  flag: number;
  user_image = '';
  static notificationCount: number = 0

  getCount() {
    return TemplateComponent.notificationCount;
  }

  ngOnInit() {
    //jQuery to open and close the dropdown
    $(document).ready(function () {
      var dropdown = document.getElementsByClassName("dropdown-btn");
      var i;
      for (i = 0; i < dropdown.length; i++) {
        dropdown[i].addEventListener("click", function () {
          this.classList.toggle("active");
          var dropdownContent = this.nextElementSibling;
          if (dropdownContent.style.display === "block") {
            dropdownContent.style.display = "none";
          } else {
            dropdownContent.style.display = "block";
          }
        });
      }

    });

    //Get user data
    this.httpuserservice.getData()
      .subscribe(
        (res) => {
          this.user_name = res.val[0].user_name;
          TemplateComponent.notificationCount = res.val[0].count;
          if (res.val && res.val[0].user_image) {
            this.user_image = res.val[0].user_image.replace('?sz=50', '');
          } else {
            this.user_image = 'https://i.stack.imgur.com/34AD2.jpg';
          }
        },
        (err) => {
          console.log("err:", err)
        }
      )
  }

  //Toggle Side Bar
  toggleNav() {
    if (!this.flag) {
      document.getElementById("mySidenav").style.width = "250px";
      document.getElementById("main").style.marginLeft = "250px";
      this.flag = 1;
    } else {
      document.getElementById("mySidenav").style.width = "0px";
      document.getElementById("main").style.marginLeft = "0px";
      this.flag = null;
    }
  }
}
