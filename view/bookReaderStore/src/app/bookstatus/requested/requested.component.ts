import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Httpnotificationservice } from '../../service/httpNotificationService';
import { Httpbookservice } from '../../service/httpBookService';
import { Subject } from 'rxjs';
import { NotifierService } from 'angular-notifier';
import { DataTableDirective } from 'angular-datatables';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-requested',
  templateUrl: './requested.component.html',
  styleUrls: ['./requested.component.css']
})
export class RequestedComponent implements OnInit, OnDestroy {
  
  //Variable Declaration
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  private notifier: NotifierService;

  constructor(private httpnotificationservice: Httpnotificationservice,
    private spinnerService: Ng4LoadingSpinnerService,
    private httpbookservice: Httpbookservice,
    notifier: NotifierService) {
    this.spinnerService.show();
    this.notifier = notifier;
  }

  show: boolean = true;
  showmessage: string = "You Have not requested any book!"
  books = [{
    book_id: null,
    book_name: null,
    book_genres: null,
    book_details: null,
    request_by: null,
    user_email: null
  }];

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      retrieve: true,
      processing: true
    };

    this.httpbookservice.requestedBooks()
      .subscribe(
        res => {
          if (res.val && res.val.length) {
            this.books = res.val;
            this.dtTrigger.next();
            this.spinnerService.hide();
          } else {
            this.show = false;
          }
        },
        err => {
          this.spinnerService.hide();
          this.show = false;
        }
      )
  }

  //function responsible for canceling the request made for a perticular book
  cancelRequest(email, id) {
    this.spinnerService.show();
    this.httpnotificationservice.rejectNotification(email, id)
      .subscribe(
        res1 => {
          //Removing the perticular entry from the database and the object as well
          for (let i = 0; i < this.books.length; i++) {
            if (this.books[i].book_id == id) {
              this.books.splice(i, 1);
            }
          }
          //this part rerenders the datatable based on the values in books array of object
          this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            //Destroy the datatable
            dtInstance.destroy();

            //Manually rendering the datatable again
            this.dtTrigger.next();
          });
          this.spinnerService.hide();
          this.notifier.notify('default', 'Request Canceled');
          this.ngOnInit();
        },
        err => {
          this.spinnerService.hide();
          this.notifier.notify('error', 'Whoops, something went wrong. Probably.');
        }
      )

  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

}
