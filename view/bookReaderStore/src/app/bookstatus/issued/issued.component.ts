import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { Httpbookservice } from '../../service/httpBookService';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-issued',
  templateUrl: './issued.component.html',
  styleUrls: ['./issued.component.css']
})
export class IssuedComponent implements OnInit, OnDestroy {

  //Variable Declaration
  message = '';
  classStar = "glyphicon glyphicon-star-empty";
  constructor(private httpbookservice: Httpbookservice,
    private spinnerService: Ng4LoadingSpinnerService) {
    this.spinnerService.show();
  }

  show: boolean = true;
  showmessage: string = "No one has issued your book currently!"
  books = [{}];

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      retrieve: true,
      processing: true
    };

    this.httpbookservice.issuedBooks()
      .subscribe(
        res => {
          if (res.val && res.val.length) {
            this.books = res.val;
            this.dtTrigger.next();
            this.spinnerService.hide();
          } else {
            //else condition is responsible for showing message if the response object is empty
            this.spinnerService.hide();
            this.show = false;
          }
        },
        err => {
          this.show = false;
        }
      )
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

}
