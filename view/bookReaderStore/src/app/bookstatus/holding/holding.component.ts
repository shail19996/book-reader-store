import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Httpuserservice } from '../../service/httpUserService';
import { Httpbookservice } from '../../service/httpBookService';
import { Httpnotificationservice } from '../../service/httpNotificationService';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { Subject } from 'rxjs';
import { NotifierService } from 'angular-notifier';
import { DataTableDirective } from 'angular-datatables';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-holding',
  templateUrl: './holding.component.html',
  styleUrls: ['./holding.component.css']
})
export class HoldingComponent implements OnInit, OnDestroy {

  //Variable Declaration
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  data = {
    book_name: "",
    book_id: 0,
    email: '',
    feedback_count: null,
    feedback_comments: null,
    feedback_check: null
  };
  bookName = '';
  count = 0;
  flag = true;
  comments = '';

  // variables to set class for stars in feedback modal
  starClass1 = 'glyphicon glyphicon-star-empty glyphiconStar';
  starClass2 = 'glyphicon glyphicon-star-empty glyphiconStar';
  starClass3 = 'glyphicon glyphicon-star-empty glyphiconStar';
  starClass4 = 'glyphicon glyphicon-star-empty glyphiconStar';
  starClass5 = 'glyphicon glyphicon-star-empty glyphiconStar';
  private notifier: NotifierService;

  constructor(private httpuserservice: Httpuserservice,
    private httpnotificationservice: Httpnotificationservice,
    private spinnerService: Ng4LoadingSpinnerService,
    private httpbookservice: Httpbookservice,
    notifier: NotifierService) {
    this.notifier = notifier;
    this.spinnerService.show();
  }

  show: boolean = true;
  showmessage: string = "No books on Hand Available!"
  books = [{
    book_id: null,
    book_name: null,
    feedback_check: null,
    book_genres: null,
    book_details: null,
    user_email: null
  }];
  ownerEmail = '';
  email = '';

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      retrieve: true,
      processing: true
    };

    this.httpuserservice.check()
      .subscribe(
        res => {
          this.ownerEmail = res.val;
          this.httpbookservice.holdingBooks()
            .subscribe(
              res => {
                if (res.val && res.val.length) {
                  this.books = res.val;
                  this.dtTrigger.next();
                  this.spinnerService.hide();
                } else {
                  this.show = false;
                }
              },
              err => {
                this.spinnerService.hide();
                this.show = false;
              }
            )

        }
      )
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  modalRef: BsModalRef;
  //function responsible for resetting all the values to null
  save(bookName, bookId, ownerEmail, feedback_check) {
    this.data.feedback_count = null;
    this.data.feedback_comments = null;
    this.data.book_name = bookName;
    this.data.book_id = bookId;
    this.data.email = this.ownerEmail;
    this.data.feedback_check = feedback_check;
    this.email = ownerEmail;
    this.starClass1 = 'glyphicon glyphicon-star-empty glyphiconStar';
    this.starClass2 = 'glyphicon glyphicon-star-empty glyphiconStar';
    this.starClass3 = 'glyphicon glyphicon-star-empty glyphiconStar';
    this.starClass4 = 'glyphicon glyphicon-star-empty glyphiconStar';
    this.starClass5 = 'glyphicon glyphicon-star-empty glyphiconStar';
    this.bookName = bookName;

    //this if conditiond checks if the books is already requested for returning or not
    if (feedback_check == 'Submitted') {
      this.notifier.notify('default', 'Already Requested request is send!');
    }
  }

  //function responsible for setting values of feedback given by the user
  saveFeedback() {
    this.spinnerService.show();
    this.data.feedback_count = this.count;
    this.data.feedback_comments = this.comments;
    //function call to add data to database
    this.requestCall()
  }

  //function call when user has not given the feedback
  skipFeedback() {
    this.spinnerService.show();
    //function call to add data to database
    this.requestCall()
  }

  //function responsible for adding return request and saving the feedback
  requestCall() {
    this.httpnotificationservice.returnRequest(this.email, this.data)
      .subscribe(
        res => {
          if (this.count != 0) {
            //this for loop is to change the status of return that the feedback is given
            for (let i = 0; i < this.books.length; i++) {
              if (this.books[i].book_id == this.data.book_id && this.count != 0) {
                this.books[i].feedback_check = 'Submitted'
              }
            }
          }

          //rerendering the datatable
          this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.destroy();
            this.dtTrigger.next();
          });
          this.spinnerService.hide();
          this.notifier.notify('success', 'Successfully Requested!');
        },
        err => {
          this.spinnerService.hide();
          this.notifier.notify('error', 'Something Not Proper');
        }
      );

  }

  //function responsible for setting the classes to the star
  rating(id) {
    this.starClass1 = 'glyphicon glyphicon-star glyphiconStar';
    this.flag = false;
    this.count = id;
    1 <= id ? this.starClass1 = 'glyphicon glyphicon-star glyphiconStar glyphicon-star-gold' : this.starClass1 = 'glyphicon glyphicon-star-empty glyphiconStar';
    2 <= id ? this.starClass2 = 'glyphicon glyphicon-star glyphiconStar glyphicon-star-gold' : this.starClass2 = 'glyphicon glyphicon-star-empty glyphiconStar';
    3 <= id ? this.starClass3 = 'glyphicon glyphicon-star glyphiconStar glyphicon-star-gold' : this.starClass3 = 'glyphicon glyphicon-star-empty glyphiconStar';
    4 <= id ? this.starClass4 = 'glyphicon glyphicon-star glyphiconStar glyphicon-star-gold' : this.starClass4 = 'glyphicon glyphicon-star-empty glyphiconStar';
    5 <= id ? this.starClass5 = 'glyphicon glyphicon-star glyphiconStar glyphicon-star-gold' : this.starClass5 = 'glyphicon glyphicon-star-empty glyphiconStar';
  }

  //function responsible for checking if the Comments column contains proper number of character or not
  //and if the number exceeds than the input will be removed
  onCommentInput(event: Event) {
    let val;
    val = (<HTMLInputElement>event.target).value;
    if (val.length > 149) {
      (<HTMLInputElement>event.target).value = val.substring(0, val.length - 1);
    }
  }
}
