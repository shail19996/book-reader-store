import { Component, OnInit } from '@angular/core';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { Httpuserservice } from '../../service/httpUserService';
import { NotifierService } from 'angular-notifier';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

const URL = 'http://localhost:3000/uploadFile';
@Component({
  selector: 'app-add-books-file',
  templateUrl: './add-books-file.component.html',
  styleUrls: ['./add-books-file.component.css']
})
export class AddBooksFileComponent implements OnInit {

  private notifier: NotifierService;

  //Configuring uploader variable with max file size 1 Mb
  public uploader: FileUploader = new FileUploader({ url: URL, maxFileSize: 1024 * 1024 * 1, itemAlias: 'file' });
  constructor(private httpuserservice: Httpuserservice,
    private spinnerService: Ng4LoadingSpinnerService,
    notifier: NotifierService) {
    this.spinnerService.show();
    this.notifier = notifier;
  }

  //Variable Declaration
  progressShow: boolean = false;
  progressVal: number = 0;
  buttonFlag: boolean = true;

  ngOnInit() {
    this.httpuserservice.check()
      .subscribe(
        res => {
          this.spinnerService.hide();
          // funciton responsible for appending user email in body
          this.uploader.onBuildItemForm = (item, form) => {
            form.append('UserEmail', res.val);
          };

          //function responsible for progressbar
          this.uploader.onProgressItem = (progress: any) => {
            this.progressShow = true;
            this.progressVal = progress['progress'];
            if (this.progressVal == 100) {
              this.buttonFlag = false;
            }
          };
          this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; };

          // funciton responsible for showing proper 
          // notification after data is added to the database
          this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
            if (response.search('File Content Not Proper!') > 0)
              this.notifier.notify('error', 'File Content Not Proper!');
            if (response.search('File Format Not Proper!') > 0)
              this.notifier.notify('error', 'File Format Not Proper!');
            if (response.search('Success!!') > 0)
              this.notifier.notify('success', 'Books Added!');
          };
        }
      )
  }

  // funciton responsible for showing upload button back after file upload
  reset() {
    this.progressShow = false;
    this.progressVal = 0;
    this.buttonFlag = true;
  }
}
