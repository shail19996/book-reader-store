import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddBooksFileComponent } from './add-books-file.component';

describe('AddBooksFileComponent', () => {
  let component: AddBooksFileComponent;
  let fixture: ComponentFixture<AddBooksFileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddBooksFileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddBooksFileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
