import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { Httpuserservice } from '../../service/httpUserService';
import { Httpbookservice } from '../../service/httpBookService';
import { NotifierService } from 'angular-notifier';
import { BootstrapOptions } from '@angular/core/src/application_ref';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-add-books-form',
  templateUrl: './add-books-form.component.html',
  styleUrls: ['./add-books-form.component.css']
})
export class AddBooksFormComponent implements OnInit {
  //Variable Declaration
  buttonFlag: Boolean = true;
  private notifier: NotifierService;
  genres = [];
  book = {
    user_email: '',
    book_name: '',
    book_genres: '',
    book_details: ''
  }
  nameMessage = '';
  nameCondition: boolean = false;
  constructor(private httpuserservice: Httpuserservice,
    private spinnerService: Ng4LoadingSpinnerService,
    private httpbookservice: Httpbookservice,
    private router: Router,
    notifier: NotifierService) {
    this.spinnerService.show();
    this.notifier = notifier;
  }

  // Funcition to not save input and 
  // redirect back to the previous page
  Cancel() {
    this.router.navigate(['/ManageBooks']);
  }

  // function responsible for updating data to database
  Save() {
    if (this.book.book_name) {
      this.spinnerService.show();
      this.httpbookservice.addByForm(this.book)
        .subscribe(
          res => {
            this.book.book_name = '';
            this.book.book_genres = '';
            this.book.book_details = '';
            this.spinnerService.hide();
            this.notifier.notify('success', 'Book Added!!');
          },
          err => {
          this.spinnerService.hide();
            this.notifier.notify('error', 'Whoops, something went wrong. Probably.');
          }
        )
    } else {
      // else part is called when disabled is removed from html page 
      // and book_name is not given
      this.nameMessage = "Book Name Is Required";
      this.nameCondition = true;
      setTimeout(() => {
        this.nameCondition = false;
      }, 2000);
    }
  }
  ngOnInit() {
    this.httpuserservice.check()
      .subscribe(
        res => {
          this.book.user_email = res.val;
          this.spinnerService.hide();
        }
      )
    this.genres = ['Science fiction', 'Satire', 'Drama', 'Action and Adventure', 'Romance', 'Mystery', 'Horror', 'Self help', 'Health', 'Guide', 'Travel', 'Children\'s', 'Religion, Spirituality & New Age', 'Science', 'History', 'Math', 'Anthology', 'Poetry', 'Encyclopedias', 'Dictionaries', 'Comics', 'Art', 'Cookbooks', 'Diaries', 'Journals', 'Prayer books', 'Series', 'Trilogy', 'Biographies', 'Autobiographies', 'Fantasy', 'Comedy'];
    this.genres = this.genres.sort();
  }

  //function responsible for checking if the Deails column contains proper number of character or not
  //and if the number exceeds than the input will be removed
  onDetailsInput(event: Event) {
    let val;
    val = (<HTMLInputElement>event.target).value;
    if (val.length > 1400000000) {
      (<HTMLInputElement>event.target).value = val.substring(0, val.length - 1);
    }

  }

  //function responsible for checking if the Book Name column contains proper number of character or not
  //and if the number exceeds than the input will be removed
  onNameInput(event: Event) {
    let val;
    val = (<HTMLInputElement>event.target).value;
    this.buttonFlag = false;
    if (val.length > 44) {
      (<HTMLInputElement>event.target).value = val.substring(0, val.length - 1);
    }
    if (val.length == 0) {
      this.buttonFlag = true;
    }
  }
}
