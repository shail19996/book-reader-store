import { Component, OnInit, AfterViewInit, OnDestroy, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Httpbookservice } from '../../service/httpBookService';
import { Subject } from 'rxjs';
import { NotifierService } from 'angular-notifier';
import { Router } from '@angular/router'
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
@Component({
  selector: 'app-manage-book',
  templateUrl: './manage-book.component.html',
  styleUrls: ['./manage-book.component.css']
})
export class ManageBookComponent implements OnInit, AfterViewInit, OnDestroy {

  //Variable Declaration
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  private notifier: NotifierService;

  constructor(private httpbookservice: Httpbookservice,
    private router: Router,
    private spinnerService: Ng4LoadingSpinnerService,
    notifier: NotifierService) {
    this.notifier = notifier;
    this.spinnerService.show();
  }

  show: boolean = true;
  showmessage: string = "No Books on Hand Available"
  books = [{
    book_id: null,
    book_name: null,
    book_genres: null,
    book_details: null,
    book_taken: null,
    user_email: null
  }];
  genres = [];
  updateVal = {
    book_name: null,
    book_genres: null,
    book_details: null
  };
  flag: boolean = false;
  deleteID = null;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      retrieve: true,
      processing: true
    };

    this.updateVal = {
      book_name: null,
      book_genres: null,
      book_details: null
    };

    this.httpbookservice.getBooks()
      .subscribe(
        res => {
          if (res.val && res.val.length) {
            this.books = res.val;
            this.spinnerService.hide();
          } else {
            //else part hides the table if the res 
            // object is empty and shows proper message            
            this.spinnerService.hide();
            this.show = false;
          }
        },
        err => {
          //else part hides the table if the any error 
          // occures during the time of getting data
          this.show = false;
        }
      )
    this.genres = ['Science fiction', 'Satire', 'Drama', 'Action and Adventure', 'Romance', 'Mystery', 'Horror', 'Self help', 'Health', 'Guide', 'Travel', 'Children\'s', 'Religion, Spirituality & New Age', 'Science', 'History', 'Math', 'Anthology', 'Poetry', 'Encyclopedias', 'Dictionaries', 'Comics', 'Art', 'Cookbooks', 'Diaries', 'Journals', 'Prayer books', 'Series', 'Trilogy', 'Biographies', 'Autobiographies', 'Fantasy', 'Comedy'];
    this.genres = this.genres.sort();
  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  //function responsible for copying the id of book to be deleted
  del(id) {
    this.deleteID = id;
  }

  //function responsible for setting flags which help in inline editing
  updateBook(newbook) {
    this.updateVal = newbook;
    this.flag = true;
    if (!this.updateVal.book_genres) {
      this.updateVal.book_genres = null;
    }
    if (!this.updateVal.book_details) {
      this.updateVal.book_details = null;
    }
  }

  //function responsible for deleting book from database 
  // based on the id found in del() function
  deleteBook() {
    this.spinnerService.show();
    this.httpbookservice.deleteBooks(this.deleteID)
      .subscribe(
        res => {
          for (let i = 0; i < this.books.length; i++) {
            if (this.books[i].book_id == this.deleteID) {
              this.books.splice(i, 1);
            }
          }
          this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.destroy();
            this.dtTrigger.next();
          });
          this.spinnerService.hide();
          this.notifier.notify('success', 'Book Deleted!')
        }, err => {
          this.spinnerService.hide();
          this.notifier.notify('error', 'Whoops, something went wrong. Probably.');
        }
      )
  }

  //function responsible for saving the updated data
  saveBook() {
    this.spinnerService.show();
    console.log("this.updateVal", this.updateVal, this.updateVal.book_name.length)
    if (this.updateVal.book_name.length == 0 || this.updateVal.book_name.length > 45) {
      this.notifier.notify('error', 'Book name is necessary and can contain only 45 Characters');
    } else {
      this.httpbookservice.updateBooks(this.updateVal)
        .subscribe(
          res => {
            this.updateVal = {
              book_name: null,
              book_genres: null,
              book_details: null
            };
            this.flag = false;
            this.notifier.notify('success', 'Book Data Updated!');
            this.spinnerService.hide();
          },
          err => {
            console.log("Error:", err)
            this.spinnerService.hide();
            this.notifier.notify('error', 'Whoops, something went wrong. Probably.');
          }
        )
    }
  }

  //function responsible for calling the file upload page
  importFileCall() {
    this.router.navigate(['/AddBooksFile']);
  }

  //function responsible for calling the form which helps in adding new files
  formCall() {
    this.router.navigate(['/AddBooksForm']);
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }
}
