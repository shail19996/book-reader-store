import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Httpfeedbackservice } from '../service/httpFeedbackService';
import { Subject } from 'rxjs';
import { Router } from '@angular/router'
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-read-comments',
  templateUrl: './read-comments.component.html',
  styleUrls: ['./read-comments.component.css']
})
export class ReadCommentsComponent implements OnInit {

  //Variable Declaration
  id: number;
  back: string;
  comments = [];
  private sub: any;
  constructor(private Httpfeedbackservice: Httpfeedbackservice,
    private spinnerService: Ng4LoadingSpinnerService,
    private route: ActivatedRoute,
    private router: Router) {
    this.spinnerService.show();
  }


  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();

  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      processing: true,
      // searching: false
    };
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id'];
      this.back = params['last'];
    });
    this.Httpfeedbackservice.getComments(this.id)
      .subscribe(
        res => {
          this.comments = res.val;
          console.log(this.comments)
          this.dtTrigger.next();
          this.spinnerService.hide();
        },
        err => {
          console.log(err)
        }
      )

  }

  lastPage() {
    this.router.navigate(['/' + this.back]);
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
