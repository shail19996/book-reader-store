import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { Httpfeedbackservice } from './service/httpFeedbackService'
import { Httpuserservice } from './service/httpUserService';
import { Httpnotificationservice } from './service/httpNotificationService';
import { Httpbookservice } from './service/httpBookService';
import { LoginComponent } from './login/login.component';
import { HttpModule } from '@angular/http';
import { HomepageComponent } from './homepage/homepage.component';
import { LogoutComponent } from './logout/logout.component';
import { NotificationComponent } from './notification/notification.component';
import { ProfileComponent } from './profile/profile.component';
import { FormsModule } from '@angular/forms';
import { AuthGuard } from './auth-guard.service';
import { AddBooksFormComponent } from './books/add-books-form/add-books-form.component';
import { AddBooksFileComponent } from './books/add-books-file/add-books-file.component';
import { RequestedComponent } from './bookstatus/requested/requested.component';
import { IssuedComponent } from './bookstatus/issued/issued.component';
import { HoldingComponent } from './bookstatus/holding/holding.component';
import { FeedbackComponent } from './feedback/feedback.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { DataTablesModule } from 'angular-datatables';
import { TemplateComponent } from './template/template.component';
import { ManageBookComponent } from './books/manage-book/manage-book.component';
import { FileSelectDirective } from 'ng2-file-upload';
import { Ng5SliderModule } from 'ng5-slider';
import { ConfigModule } from './app.config';
import { CommonModule } from '@angular/common';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { ReadCommentsComponent } from './read-comments/read-comments.component';
import { ChatComponent } from './chat/chat.component';
import { ChatService } from './service/chat.service';
import { Httpchatservice } from './service/httpChatService';
import {EmojiPickerModule} from 'ng-emoji-picker';


//Routes
const appRoutes: Routes = [
    { path: '', component: LoginComponent },
    { path: 'Home', canActivate: [AuthGuard], component: HomepageComponent },
    { path: 'Logout', component: LogoutComponent },
    { path: 'Notification', canActivate: [AuthGuard], component: NotificationComponent },
    { path: 'Profile', canActivate: [AuthGuard], component: ProfileComponent },
    { path: 'AddBooksForm', canActivate: [AuthGuard], component: AddBooksFormComponent },
    { path: 'AddBooksFile', canActivate: [AuthGuard], component: AddBooksFileComponent },
    { path: 'ManageBooks', canActivate: [AuthGuard], component: ManageBookComponent },
    { path: 'Request', canActivate: [AuthGuard], component: RequestedComponent },
    { path: 'Issue', canActivate: [AuthGuard], component: IssuedComponent },
    { path: 'Holding', canActivate: [AuthGuard], component: HoldingComponent },
    { path: 'Feedback', canActivate: [AuthGuard], component: FeedbackComponent },
    { path: 'ReadComment/:last/:id', canActivate: [AuthGuard], component: ReadCommentsComponent },
    { path: 'Chat', canActivate: [AuthGuard], component: ChatComponent },
    { path: '**', canActivate: [AuthGuard], component: HomepageComponent }
];

@NgModule({
    declarations: [
        LoginComponent,
        NotificationComponent,
        HomepageComponent,
        LogoutComponent,
        ProfileComponent,
        AddBooksFormComponent,
        AddBooksFileComponent,
        RequestedComponent,
        IssuedComponent,
        HoldingComponent,
        FeedbackComponent,
        TemplateComponent,
        ManageBookComponent,
        ReadCommentsComponent,
        ChatComponent,
        FileSelectDirective
    ],
    imports: [
        CommonModule,
        ConfigModule,
        FormsModule,
        HttpModule,
        HttpClientModule,
        DataTablesModule,
        Ng5SliderModule,
        EmojiPickerModule,
        ModalModule.forRoot(),
        Ng4LoadingSpinnerModule.forRoot(),
        RouterModule.forRoot(appRoutes)
    ],
    providers: [
        Httpfeedbackservice,
        Httpuserservice,
        Httpnotificationservice,
        Httpbookservice,
        AuthGuard,
        ChatService,
        Httpchatservice
    ],
    exports: [
        RouterModule
    ]
})
export class RoutingModule { };