import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Httpbookservice } from '../service/httpBookService';
import { Subject } from 'rxjs';
import { NotifierService } from 'angular-notifier';
import { DataTableDirective } from 'angular-datatables';
import { Httpfeedbackservice } from '../service/httpFeedbackService';
import { Options } from 'ng5-slider';
import { Router } from '@angular/router'
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})

export class HomepageComponent implements OnInit, OnDestroy {

  //Variable Declaration
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  private notifier: NotifierService;

  constructor(private httpbookservice: Httpbookservice,
    private spinnerService: Ng4LoadingSpinnerService,
    private Httpfeedbackservice: Httpfeedbackservice,
    private router: Router,
    notifier: NotifierService) {
    this.spinnerService.show();
    this.notifier = notifier;
  }
  modalShow: boolean = false;
  flag = 0;
  FilterShow: boolean = true;
  value: number = 0;
  highValue: number = 5;
  options: Options = {
    floor: 0,
    ceil: 5
  };
  show: boolean = true;
  showmessage: string = "Sorry but no books are currently available. All the books are already issued"
  books = [{
    book_id: null,
    book_name: null,
    book_genres: null,
    book_details: null,
    user_email: null,
    avg: null
  }];
  genres = [];
  filterGenres = null;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      retrieve: true,
      processing: true
    };
    this.httpbookservice.getAvailableBooks(this.value, this.highValue, this.filterGenres)
      .subscribe(
        res => {
          if (res.val && res.val.length != 0) {
            this.show = true;
            this.books = res.val;
            //if else to check if the datatable needs to be rerendered or not
            if (this.flag == 1) {
              //it needs to be rerendered
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.dtTrigger.next();
              });
            }
            else {
              //no need of rerender
              this.dtTrigger.next();
            }

            //helps in finding unique genres for the filter dropdown
            this.genres = res.val.map(item => item.book_genres)
              .filter((value, index, self) => self.indexOf(value) === index)

            //removes blank spaces in the genre array
            this.genres = this.genres.filter(function (entry) { return entry.trim() != ''; });
            this.spinnerService.hide();
          } else {
            //called when the response object is empty
            this.books = null;
            //if else to check if the datatable needs to be rerendered or not
            if (this.flag == 1) {
              //it needs to be rerendered
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.dtTrigger.next();
              });
            }
            else {
              //no needs of rerendered
              this.dtTrigger.next();
            }
            this.spinnerService.hide();
            this.show = false;
          }
        },
        err => {
          this.show = false;
          this.spinnerService.hide();
        }
      )
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  requestBook(id) {
    this.spinnerService.show();
    this.httpbookservice.requestBook(id)
      .subscribe(
        res => {
          //removes the requested book form the list
          for (let i = 0; i < this.books.length; i++) {
            if (this.books[i].book_id == id) {
              this.books.splice(i, 1);
            }
          }
          //helps in rendering the datatable
          this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            //destroy datatable
            dtInstance.destroy();

            //manually create table once again
            this.dtTrigger.next();
          });
          this.spinnerService.hide();
          this.notifier.notify('success', 'Book Requested!');
        },
        err => {
          this.notifier.notify('default', 'This book is already Requested By you!');
        }
      )
  }

  popupErrorMessage = 'No Comments to Read!';
  showComments(id) {
    this.modalShow = false;
    this.Httpfeedbackservice.getComments(id)
      .subscribe(
        res => {
          //redirected to a new page to read comments
          this.router.navigate(['/ReadComment/Home', id]);
        }, err => {
          this.modalShow = true;
        }
      )
  }

  //function to clear all the filters applied
  checkFilter() {
    this.FilterShow = true;
    this.value = 0;
    this.highValue = 5;
    this.filterGenres = null;
    this.spinnerService.show();
    this.ngOnInit();
  }

  //function to apply filters 
  applyFilter() {
    this.flag = 1;
    if (this.filterGenres == "") {
      this.filterGenres = null;
    }
    this.FilterShow = false;
    this.spinnerService.show();
    this.ngOnInit();
  }
}
