import { Injectable } from '@angular/core'
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable()
export class Httpbookservice {
    headers = new HttpHeaders({
        'Content-Type': 'application/json',
    });
    constructor(private http: HttpClient) { }

    url: string = 'http://localhost:3000/';
    //Add book based on the form filling
    addByForm(data) {
        return this.http.post(this.url + 'addByForm', data, {
            withCredentials: true,
            headers: this.headers
        }
        );
    }

    //Show requestable books for a perticular user
    getAvailableBooks(min, max, genre) {
        return this.http.get<any>(this.url + 'getAvailableBooks/' + max + "/" + min + "/" + genre, { withCredentials: true });
    }

    //Request Books
    requestBook(id) {
        return this.http.get<any>(this.url + 'requestBook/' + id, { withCredentials: true });
    }

    //Show Requested Books
    requestedBooks() {
        return this.http.get<any>(this.url + 'requestedBooks', { withCredentials: true });
    }

    //Shows the current holder of the book to the owner
    issuedBooks() {
        return this.http.get<any>(this.url + 'issuedBooks', { withCredentials: true });
    }

    //Show Book on hand which includes both requested books and own books not given to anyone
    holdingBooks() {
        return this.http.get<any>(this.url + 'holdingBooks', { withCredentials: true });
    }

    //Get all the available books
    getBooks() {
        return this.http.get<any>(this.url + 'getBooks', { withCredentials: true });
    }

    //Delete Book record from database
    deleteBooks(id) {
        return this.http.delete<any>(this.url + 'books/' + id, { withCredentials: true });
    }

    //Update Book Details
    updateBooks(data) {
        return this.http.put<any>(this.url + 'books', data, {
            withCredentials: true,
            headers: this.headers
        }
        );
    }
}