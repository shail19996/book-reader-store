import { Injectable } from '@angular/core'
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable()
export class Httpuserservice {
    headers = new HttpHeaders({
        'Content-Type': 'application/json',
    });
    constructor(private http: HttpClient) { }

    url: string = 'http://localhost:3000/';
    //Destroy user session
    logout() {
        return this.http.get(this.url + 'logout', { withCredentials: true });
    }

    //check if user is loggedin or not
    check() {
        return this.http.get<any>(this.url + 'check', { withCredentials: true });
    }

    //Get data to represent on the templete
    getData() {
        return this.http.get<any>(this.url + 'getData', { withCredentials: true });
    }

    //Update User Info
    updateData(data) {
        return this.http.put(this.url + 'updateData', data, {
            withCredentials: true,
            headers: this.headers
        }
        );
    }

}