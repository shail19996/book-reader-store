import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http';

@Injectable()
export class Httpfeedbackservice {

    constructor(private http: HttpClient) { }

    url: string = 'http://localhost:3000/';
    //Check is feedback is given or not
    checkFeedback(email, id) {
        return this.http.get<any>(this.url + 'checkFeedback/' + email + "/" + id, { withCredentials: true });
    }

    //return reating of books
    getFeedback() {
        return this.http.get<any>(this.url + 'getFeedback', { withCredentials: true });
    }

    //returns comments of perticular book if any
    getComments(id) {
        return this.http.get<any>(this.url + 'getComments/' + id, { withCredentials: true });
    }
}