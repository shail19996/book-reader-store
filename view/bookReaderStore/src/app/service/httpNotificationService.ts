import { Injectable } from '@angular/core'
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable()
export class Httpnotificationservice {
    headers = new HttpHeaders({
        'Content-Type': 'application/json',
    });
    constructor(private http: HttpClient) { }

    url: string = 'http://localhost:3000/';
    //Get all Notification
    getNotification() {
        return this.http.get<any>(this.url + 'getNotification', { withCredentials: true });
    }

    //Reject book request
    rejectNotification(email, id) {
        return this.http.get<any>(this.url + 'rejectNotification/' + email + "/" + id, { withCredentials: true });
    }

    //accept book request
    acceptNotification(email, id) {
        return this.http.get<any>(this.url + 'acceptNotification/' + email + "/" + id, { withCredentials: true });
    }

    //Book received back
    returnRequest(email, data) {
        return this.http.post(this.url + 'returnRequest/' + email, data, {
            withCredentials: true,
            headers: this.headers
        }
        );
    }
    acceptReturnRequest(email, id) {
        return this.http.get<any>(this.url + 'acceptReturnRequest/' + email + "/" + id, { withCredentials: true });
    }
}