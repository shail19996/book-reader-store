import { Injectable } from '@angular/core'
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable()
export class Httpchatservice {
    headers = new HttpHeaders({
        'Content-Type': 'application/json',
    });
    constructor(private http: HttpClient) { }

    url: string = 'http://localhost:3000/';
    //Get Chat data
    getChat(offset) {
        return this.http.get<any>(this.url + 'chat/' + offset, { withCredentials: true });
    }

    deleteChat(id) {
        return this.http.delete<any>(this.url + 'chat/' + id, { withCredentials: true });
    }
}