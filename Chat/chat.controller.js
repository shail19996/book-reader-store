const model = require('./chat.model');

module.exports = {
    postChat,
    getChat,
    deleteChat
};

// Function to Insert message
function postChat(message) {
    model.postChat(message);
}

// Function to get Chat History
async function getChat(req, res) {
    try {
        const offset = parseInt(req.params.offset, 10);
        const response = await model.getChat(offset);
        res.status(200).json({ val: response })
    } catch (err) {
        res.status(400).json({ err: err });
    }
}

// Function to delete Message
async function deleteChat(req, res) {
    try {
        const response = await model.deleteChat(req.params.id);
        res.status(200).json({ val: response })
    } catch (err) {
        res.status(400).json({ err: err });
    }
}