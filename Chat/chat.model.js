const Promise = require('promise');
const squel = require("squel");
const db = require('../dbConnection');

module.exports = {
    postChat,
    getChat,
    deleteChat
};


async function postChat(message) {
    let re = new RegExp("'", 'g');
    const insertChat = squel.insert()
        .into("books.chat")
        .set("chat_id", message.chat_id)
        .set("message", message.message.replace(re, "\\'"))
        .set("sender", message.sender)
        .set("date", message.date)
        .set("time", message.time)
        .toString()
    db.poolQueryPromised(insertChat);
}

async function getChat(offset) {
    try {
        const limit = 10;
        // var selectChat = squel.select()
        //     .from("books.chat")
        //     .limit(limit)
        //     .offset(offset)
        //     .order("date", false)
        //     .order("time", false)
        //     .toString()
        var selectChat = squel.select()
            .from("books.chat")
            .field('books.chat.chat_id')
            .field('books.chat.message')
            .field('books.chat.sender')
            .field('books.chat.date')
            .field('books.chat.time')
            .field('books.user.user_image')
            .join("books.user", null, 'books.chat.sender = books.user.user_email')
            .limit(limit)
            .offset(offset)
            .order("date", false)
            .order("time", false)
            .toString()
        let res1 = await db.poolQueryPromised(selectChat);
        return res1;
    }
    catch (err) {
        console.log(err);
        console.log("getChat")
    }
}

async function deleteChat(id) {
    try {
        var deleteChat = squel.delete()
            .from("books.chat")
            .where("chat_id = '" + id + "'")
            .toString()
        await db.poolQueryPromised(deleteChat);
        return res1;
    } catch (err) {
        console.log(err)
        console.log("deleteChat")
    }
}