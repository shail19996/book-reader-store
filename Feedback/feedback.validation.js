const Joi = require('joi');

module.exports = {
    getComments,
    checkFeedback
}

//Schema
const getComments_Schema = Joi.object().keys({
    id: Joi.number().min(1).required()
})

const checkFeedback_Schema = Joi.object().keys({
    id: Joi.number().min(1).required(),
    email: Joi.string().email().required()
})

//Common Function
function commmonFunctionCall(objectValue, schemaValue, res, next) {
    const result = Joi.validate(objectValue, schemaValue);
    if (result.error) {
        res.status(400).json({ err: result.error.details[0].message });
        res.end;
    }
    else {
        next();
    }
}

//Functions to validate input data
function getComments(req, res, next) {
    commmonFunctionCall(req.params, getComments_Schema, res, next);
}

function checkFeedback(req, res, next) {
    commmonFunctionCall(req.params, checkFeedback_Schema, res, next);
}