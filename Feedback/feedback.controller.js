const model = require('./feedback.model');

module.exports = {
    checkFeedback,
    getFeedback,
    getComments
};


// Function to Check if feedback is given or not
async function checkFeedback(req, res) {
    try {
        const response = await model.checkFeedback(req.params.email, req.params.id);
        res.status(200).json({ val: response })
    } catch (err) {
        res.status(400).json({ err: err });
    }
}

// Function to Get Feedback and respective rating of books
async function getFeedback(req, res) {
    try {
        const response = await model.getFeedback(req.user);
        res.status(200).json({ val: response })
    } catch (err) {
        res.status(400).json({ err: err });
    }
}

// Function to Get Comments of a specific book
async function getComments(req, res) {
    try {
        const response = await model.getComments(req.params.id);
        res.status(200).json({ val: response })
    } catch (err) {
        res.status(400).json({ err: err });
    }
}