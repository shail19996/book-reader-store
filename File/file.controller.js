const multer = require('multer');

module.exports = {
    downloadFile,
    uploadFile
};

// Defining the location and file name of the uploading file
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, '/home/shail/Desktop/demo/book-reader-store/public')
    },
    filename: (req, file, cb) => {
        let ex = file.originalname.split('.').pop();
        cb(null, file.fieldname + '-' + Date.now() + '.' + ex)
    }
});

const upload = multer({ storage: storage }).single('file');

// Function to Upload file and forward request to addByFile function
function uploadFile(req, res, next) {
    upload(req, res, function (err) {
        if (err) {
            return res.end("Error uploading file." + err);
        } else {
            req.fileName = req.file.filename;
            next();
        }
    });
}

// Function to Download the temolate of .xlsx file
function downloadFile(req, res) {
    var url = './public/template.xlsx'
    res.download(url, 'template.xlsx', function (err) {
        if (err) {
            console.log('something went wrong:', err);
        }
    });
}