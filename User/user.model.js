const Promise = require('promise');
const squel = require("squel");
const db = require('../dbConnection');

module.exports = {
    login,
    getData,
    updateData
};

async function login(user) {
    try {
        // query for checking if user in present in database or not
        let selectUser = squel.select()
            .from("books.user")
            .where("user_email = '" + user.emails[0].value + "'")
            .toString()
        var res1 = await db.poolQueryPromised(selectUser);
        if (res1.length === 0) {
            // query for adding user to database
            let insertUser = squel.insert()
                .into("books.user")
                .set("user_name", "'" + user.displayName + "'")
                .set("user_email", "'" + user.emails[0].value + "'")
                .set("user_image", "'" + user.photos[0].value + "'")
                .toString()
            await db.poolQueryPromised(insertUser);
            return user.emails[0].value;
        }
        else {
            // return user email for session value
            return user.emails[0].value;
        }
    } catch (err) {
        console.log("Error In: login");
        throw new Error(err);
    }
}

async function getData(email) {
    try {
        // query for getting user detail
        var selectUser = squel.select()
            .from("books.user")
            .where("user_email = '" + email + "'")
            .toString()
        var res1 = await db.poolQueryPromised(selectUser);
        if (res1.length === 0) {
            return Promise.reject("Invalid ID");
        }
        else {
            // query for getting notification count
            var selectNotification = squel.select()
                .from("books.notification")
                .field("COUNT(*)", "count")
                .where("notification_to = '" + email + "'")
                .where("visible = 'true'")
                .toString()

            var count = await db.poolQueryPromised(selectNotification);
            res1[0].count = count[0].count;
            return res1;
        }
    } catch (err) {
        if (err = "Invalid ID") {
            return Promise.reject("Invalid ID");
        }
        console.log("Error In: getData");
        throw new Error(err);
    }
}

async function updateData(email, body) {
    var size = Object.keys(body).length;
    if (size) {
        // re for appending \\ before all ' in the input
        let re = new RegExp("'", 'g');
        // query for updating user detail
        var query = "UPDATE books.user SET ";
        var i = 1;
        if (body.user_mobile == '') {
            body.user_mobile = null;
        }
        for (key in body) {
            if (key === 'user_mobile') {
                query = query + key + " = " + body[key];
            } else {
                query = query + key + " = '" + body[key].replace(re, "\\'") + "'";
            }
            if (i != size) {
                // If condition so that the query doesnot add ',' in the last entry
                // Otherwise it will be ', WHERE user_email' which is an error
                query = query + " ,";
            }
            i++;
        }
        query = query + " WHERE user_email = '" + email + "'";
        try {
            await db.poolQueryPromised(query);
            return "Success!!";
        } catch (err) {
            console.log("Error In: updateData");
            throw new Error(err);
        }
    } else {
        return "No Input";
    }
}
