const model = require('./user.model');

module.exports = {
    login,
    getData,
    updateData
};

// Function to check is user loggedin is registered in the database or not
// if not than add it to the database
async function login(req, res) {
    try {
        await model.login(req.user);
        res.redirect('http://localhost:4200/Home');
    } catch (err) {
        res.status(400).json({ err: err });
    }
}

// Function to get basic details for templete
async function getData(req, res) {
    try {
        const response = await model.getData(req.user);
        res.status(200).json({ val: response })
    } catch (err) {
        res.status(400).json({ err: err });
    }
}


// Function to Update user details
async function updateData(req, res) {
    try {
        const response = await model.updateData(req.user, req.body);
        res.status(200).json({ val: response })
    } catch (err) {
        res.status(400).json({ err: err });
    }
}